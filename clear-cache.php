<?php

use Nette\Utils\FileSystem,
    Nette\Utils\Finder;

require '@vendor/nette/utils/src/Utils/Object.php';
require '@vendor/nette/utils/src/Iterators/Filter.php';
require '@vendor/nette/utils/src/Utils/Callback.php';
require '@vendor/nette/utils/src/Utils/FileSystem.php';
require '@vendor/nette/finder/src/Finder/Finder.php';

FileSystem::delete('@temp/cache');
FileSystem::delete('@temp/sessions');
FileSystem::delete('@temp/btfj.dat');
FileSystem::delete('@temp/web.config');

die('deleted');
