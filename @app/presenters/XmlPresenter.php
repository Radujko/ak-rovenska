<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class XmlPresenter extends BasePresenter
{


	public function actionDefault()
	{
		$this->template->products = array('carlaineSalt', 'carlaineTablets', 'inferum', 'rofes');
		$this->template->langs = array('cz');
	}

}
