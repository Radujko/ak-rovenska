<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class ReferencePresenter extends BasePresenter
{


	public function actionDefault()
	{
		$this->template->references = $this->reference->fullReference();
	}


	public function actionDetail($id, $name)
	{
		if (!$id) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		$reference = $this->reference->getFullReference($id);

		if (!$reference) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		$headline = Nette\Utils\Strings::webalize($reference->headline);

		if (!$name || $name != $headline) {

			$this->redirect(301, 'Reference:detail', array('id' => $reference->id, 'name' => $headline));
		}

		$this->template->reference = $reference;
	}

}
