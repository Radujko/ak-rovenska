<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class ArticlesPresenter extends BasePresenter
{

	/** @var \App\Model\PageData @inject */
	public $pageData;


	public function actionDefault($lang,$pageUrl)
	{

		$page = $this->pageData->getPageByUrl($pageUrl);

		if (!$page) { // #404
			throw new \Exception('Stránka nenalezena.', 404);
		}

		$pageData = $this->pageData->getPageDataByKey($page->id);
		$prilohy = $this->pageData->getPrilohyByKey($page->id);
		$gallery = $this->pageData->getGalleryByKey($page->id);
		$sideNews = $this->news->fullNews(2, false, false);
		$breadcrumb = $this->pageData->getBreadcrumb($page->id, 54);

		if ($pageData->urlToRedirect) {
			$this->redirectUrl($pageData->urlToRedirect, 301);
		}

		$this->template->page = $page;
		$this->template->pageData = $pageData;
		$this->template->prilohy = $prilohy;
		$this->template->gallery = $gallery;
		$this->template->sideNews = $sideNews;
		$this->template->breadcrumb = $breadcrumb;

	}

	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}

	public function actionDetail($id, $name)
	{
		if (!$id) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		$new = $this->news->getFullNew($id);


		if (!$new) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		$lang = $this->lang;

		$headline = Nette\Utils\Strings::webalize($new->headline_cz);

		
		if (!$name || $name != $headline) {
			$this->redirect(303, 'Articles:detail', array('id' => $new->id, 'name' => $headline));
		}

		$this->template->new = $new;

	}
}
