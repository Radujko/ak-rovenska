<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class EventsPresenter extends BasePresenter
{

	/** @var \App\Model\PageData @inject */
	public $pageData;


	public function actionDefault($pageNumber = 1)
	{
		$count = $this->news->countMaxNews();

		$paginator =  new Nette\Utils\Paginator;
		$paginator->setItemCount($count);
		$paginator->setItemsPerPage(8);
		$paginator->setPage($pageNumber);

		$this->template->news = $this->news->fullNews(false,false,false,$paginator,true);
		$this->template->paginator = $paginator;

		$page = $this->pageData->getPageByUrl($this->params['pageUrl']);
		$pageData = $this->pageData->getPageDataByKey($page->id);

		$this->template->page = $page;
		$this->template->pageData = $pageData;
		$this->template->breadcrumb = $this->pageData->getBreadcrumb($page->id, 54);
	}

	public function actionDetail($id, $url)
	{
		if (!$id) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		$new = $this->news->getFullNew($id);


		if (!$new) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		if (!$new->visible) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		$lang = $this->lang;

		$headline = Nette\Utils\Strings::webalize($new->headline_cz);

		
//		if (!$url || $url != $headline) {
//			$this->redirect(303, 'News:detail', array('id' => $new->id, 'name' => $headline));
//		}

		$this->template->new = $new;
		$this->template->coverImage = $this->news->getCoverImage($id);

		$this->template->news = $this->news->fullNews(4,false,false);

		$page = $this->pageData->getPageByUrl($this->params['pageUrl']);
		$pageData = $this->pageData->getPageDataByKey($page->id);

		$this->template->page = $page;
		$this->template->pageData = $pageData;
		$this->template->breadcrumb = $this->pageData->getBreadcrumb($page->id, 54, 'new', $new->id);
	}



}
