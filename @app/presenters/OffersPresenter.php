<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class OffersPresenter extends BasePresenter
{


	public function actionDefault()
	{
		$this->template->jobs = $this->jobs->getJobs();
	}

	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}

}
