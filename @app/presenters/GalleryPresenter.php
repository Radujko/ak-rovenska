<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class GalleryPresenter extends BasePresenter
{

	/** @var \App\Model\PageData @inject */
	public $pageData;


	public function actionDefault($pageNumber = 1)
	{
		$this->gallery->galleryByFilter = TRUE;
		$count = $this->gallery->countMaxPages();

		$paginator = new Nette\Utils\Paginator;
		$paginator->setItemCount($count);
		$paginator->setItemsPerPage(8);
		$paginator->setPage($pageNumber);

		$this->template->gallery = $this->gallery->fullPages(false,false,false,$paginator);
		$this->template->paginator = $paginator;

		$sideNews = $this->news->fullNews(2, false, false);

		$page = $this->pageData->getPageByUrl($this->params['pageUrl']);
		$pageData = $this->pageData->getPageDataByKey($page->id);

		$this->template->page = $page;
		$this->template->sideNews = $sideNews;
		$this->template->pageData = $pageData;
		$this->template->breadcrumb = $this->pageData->getBreadcrumb($page->id, 54);
	}

	public function actionDetail($id, $url)
	{
		if (!$id) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		$gpage = $this->gallery->getFullPage($id);


		if (!$gpage) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		if (!$gpage->visible) {
			$this->setView('404');
			throw new Nette\Application\BadRequestException();
		}

		$lang = $this->lang;

		$headline = Nette\Utils\Strings::webalize($gpage->headline_cz);

		
//		if (!$url || $url != $headline) {
//			$this->redirect(303, 'News:detail', array('id' => $gpage->id, 'name' => $headline));
//		}

		$this->template->gpage = $gpage;

		$page = $this->pageData->getPageByUrl($this->params['pageUrl']);
		$pageData = $this->pageData->getPageDataByKey($page->id);

		$this->template->page = $page;
		$this->template->pageData = $pageData;
		$this->template->breadcrumb = $this->pageData->getBreadcrumb($page->id, 54, 'gallery', $gpage->id);
	}



}
