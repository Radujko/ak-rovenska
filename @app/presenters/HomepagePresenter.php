<?php

namespace App\Presenters;

use Nette,
	App\Model;
use Nette\Utils\Strings;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

	/** @var \App\Model\PageData @inject */
	public $pageData;

	/** @var \App\Forms\ContactForm @inject */
	public $contactForm;


	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}

	public function actionDefault($lang)
	{

		$page = $this->pageData->getHomePage();

		if (!$page) { // #404
			throw new \Exception('Stránka nenalezena.', 404);
		}

		$this->template->homepageNews = $this->news->fullNews(4, false, true);
		$this->template->newsBySection = $this->news->fullNews(3, 'new', false);
		$pageData = $this->pageData->getPageDataByKey($page->id);


		$this->template->page = $page;
		$this->template->pageData = $pageData;
	}

	public function actionContact()
	{
		if (isset($this->params['pageUrl'])) {
			$page = $this->pageData->getPageByUrl($this->params['pageUrl']);
			$pageData = $this->pageData->getPageDataByKey($page->id);

			$this->template->page = $page;
			$this->template->pageData = $pageData;
		}
	}

	public function actionInfo()
	{
		if (isset($this->params['pageUrl'])) {
			$page = $this->pageData->getPageByUrl($this->params['pageUrl']);
			$pageData = $this->pageData->getPageDataByKey($page->id);

			$this->template->page = $page;
			$this->template->pageData = $pageData;
		}
	}

	public function actionOchrana()
	{
		if (isset($this->params['pageUrl'])) {
			$page = $this->pageData->getPageByUrl($this->params['pageUrl']);
			$pageData = $this->pageData->getPageDataByKey($page->id);

			$this->template->page = $page;
			$this->template->pageData = $pageData;
		}
	}

	public function actionZasady()
	{
		if (isset($this->params['pageUrl'])) {
			$page = $this->pageData->getPageByUrl($this->params['pageUrl']);
			$pageData = $this->pageData->getPageDataByKey($page->id);

			$this->template->page = $page;
			$this->template->pageData = $pageData;
		}
	}

	public function actionSearch($needle)
	{
		$page = $this->pageData->getPageByUrl($this->params['pageUrl']);
		$pageData = $this->pageData->getPageDataByKey($page->id);
		$sideNews = $this->news->fullNews(2, false, false);
		$breadcrumb = $this->pageData->getBreadcrumb($page->id, 54);
		$items = $this->search->searchArticles($needle);

		$this->template->page = $page;
		$this->template->pageData = $pageData;
		$this->template->breadcrumb = $breadcrumb;
		$this->template->sideNews = $sideNews;
		$this->template->needle = $needle;
		$this->template->items = $items;
	}


	public function actionReference()
	{
		$this->template->gallery = $this->gallery->getGalleries()->reference;
	}

	public function actionGetCalendar()
	{
		$events = [];
		$eventsSource = $this->news->getEvents();

		foreach ($eventsSource as $e) {
			$label = '';
			if ($e->label) {
				$label = 't_' . $e->label;
			}
			$events[] = [
//			    'date' => (string) date('Y-m-d H:i' ,strtotime('2016-06-30 10:00:00')),
//				'date' => (string) str_pad(strtotime('2016-06-30 11:00:00'), 13, '0', STR_PAD_RIGHT),
				'date' => (string) str_pad(strtotime($e->date), 13, '0', STR_PAD_RIGHT),
				'type' => $label,
				'description' => $e->desc,
				'title' => $e->headline,
				'url' => $this->frontApp->getPageById(64)->url . '/' . $e->id . '/' . Nette\Utils\Strings::webalize($e->headline)
			];
		}

		$this->sendResponse(new Nette\Application\Responses\JsonResponse($events));
	}

	/**
	 * Contact form.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentContactForm()
	{
		$form = $this->contactForm->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Vaše zpráva byla úspěšně odeslána.', 'success');
			$form->getPresenter()->redirect('Homepage:default');
		};
		
		return $form;
	}
}
