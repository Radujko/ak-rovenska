<?php

namespace App\Presenters;

use Nette,
	Latte,
	App\Model\SimpleTranslator,
	App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	/** @persistent */
	public $lang;

	/** @var \App\Model\Presets @inject */
	public $presets;

	/** @var \App\Model\Gallery @inject */
	public $gallery;

	/** @var \App\Model\News @inject */
	public $news;

	/** @var App\Model\SimpleTranslator */
	protected $translator;

	/** @var \App\Model\FrontApp @inject */
	public $frontApp;

	/** @var \App\Model\PageData @inject */
	public $pageData;

	/** @var \App\Forms\SearchForm @inject */
	public $searchForm;

	/** @var \App\Model\Search @inject */
	public $search;

	public $uri;

	public $baseUrl;

	public $lang_url;

	public $searchNeedle;

	


	public function startUp()
	{
		parent::startUp();

		$httpRequest = $this->context->getByType('Nette\Http\Request');
		$this->uri = $httpRequest->getUrl();
		$this->baseUrl = $this->uri->host . $this->uri->scriptPath;

		if (!isset($this->lang)) {
			$this->lang = 'cz';
			$this->lang_url = '';
		}
		if ($this->lang != 'cz') {
			$this->lang_url = $this->lang . '/';
		}

		$pageUrl = NULL;
		if (isset($this->params['pageUrl'])) {
			$pageUrl = $this->params['pageUrl'];
		}

		if (isset($this->params['needle'])) {
			$this->searchNeedle = $this->params['needle'];
		}
		$this->frontApp->lang = $this->lang;
		$this->frontApp->lang_url = $this->lang_url;
		$this->frontApp->baseUrl = $this->baseUrl;

		$this->news->lang = $this->lang; // set lang in model
		$this->news->lang_url = $this->lang_url;
		$this->news->baseUrl = $this->baseUrl;

		$this->pageData->lang = $this->lang; // set lang in model

		$this->translator = new SimpleTranslator($this->lang);
		$this->template->pageUrl = $pageUrl;
		$this->template->frontApp = $this->frontApp;
		$this->template->setTranslator($this->translator);
		$this->template->lang = $this->lang;
		$this->template->lang_url = $this->lang_url;
		$this->template->news = $this->news->fullNews();
		//$this->template->newsBySection = $this->news->getNewsBySection(3, 'new');
		$this->template->langArray = array('cz' => 'Česky','en' => 'English','de' => 'Deutsch');
		$this->template->presets = $this->presets->getPresets();
//		$this->template->page = ($this->name == 'Homepage' && $this->action == 'default') ? 'homepage' : 'subpage';

	}


	protected function createTemplate($class = NULL)
	{
		$tpl = parent::createTemplate($class);
		
		// Shortcut
		$presenter = $this;
		$tpl->current = function($pattern) use ($presenter) {
			foreach ((array) $pattern as $opt) {
				if ($presenter->isLinkCurrent($opt)) {
					return TRUE;
				}
			}
			return FALSE;
		};

		$presets = $this->presets->getPresets();
		$tpl->addFilter('version', function($path) use ($tpl, $presets) {
			$relPath = trim(Nette\Utils\Strings::replace($path, "#^{$tpl->basePath}#"), '/');
			$filePath = "$presets->wwwDir/$relPath";
			if (file_exists($filePath)) {
				return $path . '?' . filemtime($filePath);
			}
			return $path;
		});

		return $tpl;
	}

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSearchForm()
	{
		$this->searchForm->needle = $this->searchNeedle;
		$this->searchForm->actionUrl = $this->frontApp->getPageById(109)->url;
		$form = $this->searchForm->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->redirect('this');
		};
		
		return $form;
	}
}
