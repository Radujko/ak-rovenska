<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList();

		$router[] = new Route('[<lang cz|en|de|ru>/]admin/<presenter>/<action>[/<id>]', array(
			'module' => 'Admin',
			'presenter' => 'Dashboard',
			'action' => 'default',
			'id' => NULL,
			'lang' => 'cz',
		));


		$router[] = new Route('[<lang cz|en|de|ru>/]', array(
			'presenter' => 'Homepage',
			'action' => 'default',
			'pageUrl' => NULL,
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl galerie>[/<pageNumber>]', array(
			'presenter' => 'Gallery',
			'action' => 'default',
			'pageNumber' => 1,
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl galerie>/<id>/<url>', array(
			'presenter' => 'Gallery',
			'action' => 'detail',
			'id' => NULL,
			'url' => NULL,
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl aktualne>[/<pageNumber>]', array(
			'presenter' => 'News',
			'action' => 'default',
			'pageNumber' => 1,
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl aktualne>/<id>/<url>', array(
			'presenter' => 'News',
			'action' => 'detail',
			'id' => NULL,
			'url' => NULL,
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl akce>[/<pageNumber>]', array(
			'presenter' => 'Events',
			'action' => 'default',
			'pageNumber' => 1,
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl akce>/<id>/<url>', array(
			'presenter' => 'Events',
			'action' => 'detail',
			'id' => NULL,
			'url' => NULL,
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl kontakty>', array(
			'presenter' => 'Homepage',
			'action' => 'contact',
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl ochrana-osobnich-udaju>', array(
			'presenter' => 'Homepage',
			'action' => 'ochrana',
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl zasady-cookies-eu>', array(
			'presenter' => 'Homepage',
			'action' => 'zasady',
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl informace-pro-spotrebitele>', array(
			'presenter' => 'Homepage',
			'action' => 'info',
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl hledat>?needle=<needle>', array(
			'presenter' => 'Homepage',
			'action' => 'search',
			'needle' => NULL,
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<pageUrl>', array(
			'presenter' => 'Articles',
			'action' => 'default',
			'pageUrl' => NULL,
			'lang' => 'cz',
		));

		$router[] = new Route('[<lang cz|en|de|ru>/]<presenter>/<action>[/<id>]', array(
			'presenter' => 'Homepage',
			'action' => 'default',
			'id' => NULL,
			'lang' => 'cz',
		));



		return $router;
	}

}
