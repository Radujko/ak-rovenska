<?php

namespace App\AdminModule\Presenters;

use Nette,
	Latte,
	App;


class ReferencePresenter extends BasePresenter
{

	/** @var \App\Model\Reference @inject */
	public $reference;

	/** @var \App\Forms\AddReferencesForm @inject */
	public $addReferences;

	/** @var \App\Forms\AddReferenceImagesForm @inject */
	public $addReferenceImages;

	/** @var \App\Forms\EditReferenceForm @inject */
	public $editReference;


	public function actionDefault()
	{
		$this->reference->setPath($this->presets->getPresets()->path);
		$this->reference->setSizes($this->presets->getPresets()->sizes);

		$this->template->references = $this->reference->getReferences(true);
//		$this->template->fullReferences = $this->reference->fullNews();
	}

	public function actionEdit($id = null, $tab)
	{
		$this->reference->setPath($this->presets->getPresets()->path);
		$this->reference->setSizes($this->presets->getPresets()->sizes);

		$this->template->images = $this->reference->getImages($id);

		if ($this->isAjax()) {
			$tab = $this->getHttpRequest()->getPost('tab');
			$this->redirect('this', array('tab' => $tab));
		}

		if(isset($tab)) {
			$this->template->tab = $tab;	
		} else {
			$this->template->tab = 'czText';
		}
	}

	public function actionAdd($tab)
	{
		if(isset($tab)) {
			$this->template->tab = $tab;	
		} else {
			$this->template->tab = 'czText';
		}

		if ($this->isAjax()) {
			$tab = $this->getHttpRequest()->getPost('tab');
			$this->redirect('this', array('tab' => $tab));
		}
	}



	/* handlers ****************************************************/


	public function handleDeactivate($news_id, $state = 0)
	{
		$status = $this->reference->deactivateNews($news_id, $state);
		if ($status) {
			$this->flashMessage('Reference byla deaktivována.');
		} else {
			$this->flashMessage('Reference nebyla deaktivována, prosím zkuste to znovu.', 'warning');
		}
		$this->redirect('this');
	}


	public function handleSetPrimary($id, $image_id, $tab)
	{
		$status = $this->reference->primaryImage($image_id);
		$this->template->images = $this->reference->getImages($id);
		$this->redrawControl();
	}


	public function handleDeleteimage($id, $image_id)
	{

		$status = $this->reference->deleteImage($image_id);
		$this->template->images = $this->reference->getImages($id);

		if ($status) {
			$this->flashMessage('Reference byl odstraněn');
		} else {
			$this->flashMessage('Reference nebyl odstraněn, prosím zkuste to znovu.', 'warning');
		}

		$this->redrawControl();
	}

	public function actionDelete($id)
	{
		if (!$id) {
			$this->flashMessage('Vyberte platnou novinku');
			$this->redirect('Reference:default');
		}

		$status = $this->reference->deleteReference($id);

		if ((bool)$status === true) {
			$this->flashMessage('Reference byla úspěšně smazána', 'success');
			$this->redirect('Reference:default');
		} else {
			$this->flashMessage('Reference nebyla úspěšně smazána, zkuste to prosím znovu', 'warning');
			$this->redirect('Reference:default');
		}
	}



	/* factories **************************************************/


	/**
	 * AddReferences form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddReferencesForm()
	{
		$this->addReferences->setPath($this->presets->getPresets()->path);
		$lang = isset($this->params['tab']) ? $this->params['tab'] : 'czTab';
		
		$form = $this->addReferences->create($lang);
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Reference byla vložena.', 'success');
			$form->getPresenter()->redirect('Reference:');
		};
		return $form;
	}


	/**
	 * EditReference form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentEditReferenceForm()
	{
		$id = $this->params['id'];
		$lang = isset($this->params['tab']) ? $this->params['tab'] : 'czTab';
		$this->editReference->setId($id);
		$this->editReference->setPath($this->presets->getPresets()->path);
		$form = $this->editReference->create($lang);
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Reference byla upravena.', 'success');
			$form->getPresenter()->redirect('this', ['id' => $this->params['id']]);
		};
		return $form;
	}


	/**
	 * AddReferenceImages form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddReferenceImagesForm()
	{
		$id = $this->params['id'];
		$this->addReferenceImages->setId($id);

		$form = $this->addReferenceImages->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Obrázky byly nahrány.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

}
