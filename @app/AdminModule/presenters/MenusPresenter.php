<?php

namespace App\AdminModule\Presenters;

use Nette,
	Latte,
	App;


class MenusPresenter extends BasePresenter
{

	/** @var \App\Model\MenuBuilder @inject */
	public $menuBuilder;

	/** @var \App\Model\Menu @inject */
	public $menu;

	/** @var \App\Forms\AddMenuForm @inject */
	public $addMenu;

	/** @var \App\Forms\AddMenuItemForm @inject */
	public $addMenuItem;

	/** @var \App\Forms\RenameMenuForm @inject */
	public $renameMenu;

	public $editItemId;


	public function actionDefault()
	{
		$this->template->menu = $this->menu->getMenuDefault();
	}

	public function actionEdit($id)
	{
		$this->editItemId = $id;
		$this->template->editItemId = $id;

		$this->template->parentItem = $this->menu->getMenuByKey($id);
		$this->template->menu = $this->menu->getMenuEdit($id,true,true);
	}

	public function handleSaveStructure($jsonString)
	{
		if ($this->isAjax()) {
			$data = json_decode( $jsonString );
			$this->menuBuilder->table_name = 'menu';
			$out = $this->menuBuilder->saveStructure($data,$this->editItemId);
			$this->sendResponse(new Nette\Application\Responses\JsonResponse(['complete' => $out]));
		}

	}

	public function actionDelete($id, $menuId)
	{
		if (!$id) {
			$this->flashMessage('Vyberte platné menu');
			$this->redirect('Menus:default');
		}

		$status = $this->menu->deleteMenu($id);

		if ((bool)$status === true) {
			$this->flashMessage('Menu bylo úspěšně smazáno', 'success');
			if (isset($menuId)) {
				$this->redirect('Menus:edit', $menuId);
			}
			else {
				$this->redirect('Menus:');
			}
		} else {
			$this->flashMessage('Menu nebylo úspěšně smazáno, zkuste to prosím znovu', 'warning');
			if (isset($menuId)) {
				$this->redirect('Menus:edit', $menuId);
			}
			else {
				$this->redirect('Menus:');
			}
		}
	}


	/* factories **************************************************/


	/**
	 * AddPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddMenuForm()
	{
		$form = $this->addMenu->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Menu bylo přidáno.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

	/**
	 * AddPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddMenuItemForm()
	{
		$id = $this->params['id'];
		$this->addMenuItem->setId($id);

		$form = $this->addMenuItem->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Stránka přidána.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

	/**
	 * AddPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentRenameMenuForm()
	{

		$form = $this->renameMenu->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Menu přejmenováno.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

}
