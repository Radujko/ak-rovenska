<?php

namespace App\AdminModule\Presenters;

use Nette,
	Latte,
	App;


class StructurePresenter extends BasePresenter
{

	/** @var \App\Model\MenuBuilder @inject */
	public $menuBuilder;

	/** @var \App\Model\Structure @inject */
	public $structure;

	/** @var \App\Forms\AddPageStructureForm @inject */
	public $addPageStructure;

	/** @var \App\Forms\AddStructureImagesForm @inject */
	public $addStructureImages;

	/** @var \App\Forms\AddStructurePrilohyForm @inject */
	public $addStructurePrilohy;

	/** @var \App\Forms\RenamePageStructureForm @inject */
	public $renamePageStructure;

	/** @var \App\Forms\EditPageStructureGeneralForm @inject */
	public $editPageStructureGeneral;

	/** @var \App\Forms\EditPageStructureAdvancedForm @inject */
	public $editPageStructureAdvanced;

	/** @var \App\Forms\EditPageStructureMutationsForm @inject */
	public $editPageStructureMutations;

	/** @var \App\Forms\EditPageStructureSocialForm @inject */
	public $editPageStructureSocial;


	public function beforeRender()
	{
		parent::beforeRender();

		if ($this->isAjax()) {
			$this->payload->redirect = (string)$this->getHttpRequest()->getUrl();
			$this->redrawControl('strutureData');
		}
	}

	public function actionDefault()
	{
		$this->structure->setPath($this->presets->getPresets()->path);
		$this->structure->setSizes($this->presets->getPresets()->sizes);

		$this->template->menu = $this->menuBuilder->getArray(1,true);
		$this->template->menuUnsored = $this->menuBuilder->getArray(2,true);
	}

	public function handleSaveStructure($jsonString)
	{
		if ($this->isAjax()) {
			$data = json_decode( $jsonString );
			$out = $this->menuBuilder->saveStructure($data);
			$this->sendResponse(new Nette\Application\Responses\JsonResponse(['complete' => $out]));
		}

	}

	public function actionEdit($id, $tab)
	{
		$this->structure->setPath($this->presets->getPresets()->path);
		$this->structure->setSizes($this->presets->getPresets()->sizes);

		$this->template->prilohy = $this->structure->getPrilohy($id);
		$this->template->images = $this->structure->getImages($id);
		$this->template->page = $this->structure->getPageByKey($id);
		$this->template->tab = $tab;

		if ($this->isAjax()) {
			$tab = $this->getHttpRequest()->getPost('tab');
			$this->redirect('this', array('id' => $id, 'tab' => $tab));
		}

	}

	public function actionDelete($id)
	{
		if (!$id) {
			$this->flashMessage('Vyberte platnou stánku');
			$this->redirect('Structure:default');
		}

		$isUsedInMenu = $this->structure->isUsedInMenu($id);
		if ((bool)$isUsedInMenu === true) {
			$this->flashMessage('Stránku nelze smazat. Je umístěna v některém menu.', 'alert');
			$this->redirect('Structure:default');
		}

		$status = $this->structure->deletePage($id);

		if ((bool)$status === true) {
			$this->flashMessage('Stránka byla úspěšně smazána', 'success');
			$this->redirect('Structure:default');
		} else {
			$this->flashMessage('Stránka nebyla úspěšně smazána, zkuste to prosím znovu', 'warning');
			$this->redirect('Structure:default');
		}
	}




	/* handlers ****************************************************/


	public function handleSetPrimary($id, $image_id, $tab)
	{
		$status = $this->structure->primaryImage($image_id);
		$this->template->images = $this->structure->getImages($id);
		$this->redrawControl();
	}


	public function handleDeleteimage($id, $image_id)
	{

		$status = $this->structure->deleteImage($image_id);
		$this->template->images = $this->structure->getImages($id);

		if ($status) {
			$this->flashMessage('Obrázek byl odstraněn');
		} else {
			$this->flashMessage('Obrázek nebyl odstraněn, prosím zkuste to znovu.', 'warning');
		}

		$this->redrawControl();
	}


	public function handleDeletePriloha($track_id, $priloha_id)
	{

		$status = $this->structure->deletePriloha($priloha_id);
		$this->template->prilohy = $this->structure->getPrilohy($track_id);

		if ($status) {
			$this->flashMessage('Soubor byl odstraněn');
		} else {
			$this->flashMessage('Soubor nebyl odstraněn, prosím zkuste to znovu.', 'warning');
		}

		$this->redrawControl();
	}



	/* factories **************************************************/


	/**
	 * AddPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddPageStructureForm()
	{
		$form = $this->addPageStructure->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Stránka byla přidána.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

	/**
	 * RenamePageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentRenamePageStructureForm()
	{
		$form = $this->renamePageStructure->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Stránka byla přejmenována.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

	/**
	 * RenamePageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentEditPageStructureGeneralForm()
	{
		$id = $this->params['id'];
		$this->editPageStructureGeneral->setId($id);

		$form = $this->editPageStructureGeneral->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Stránka byla uložena.', 'success');
			$form->getPresenter()->redirect('this', ['id' => $this->params['id'], 'tab' => 'general']);
		};
		return $form;
	}

	/**
	 * RenamePageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentEditPageStructureAdvancedForm()
	{
		$id = $this->params['id'];
		$this->editPageStructureAdvanced->setId($id);

		$form = $this->editPageStructureAdvanced->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Stránka byla uložena.', 'success');
			$form->getPresenter()->redirect('this', ['id' => $this->params['id'], 'tab' => 'advanced']);
		};
		return $form;
	}

	/**
	 * RenamePageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentEditPageStructureSocialForm()
	{
		$id = $this->params['id'];
		$this->editPageStructureSocial->setId($id);

		$form = $this->editPageStructureSocial->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Stránka byla uložena.', 'success');
			$form->getPresenter()->redirect('this', ['id' => $this->params['id'], 'tab' => 'social']);
		};
		return $form;
	}

	/**
	 * RenamePageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentEditPageStructureMutationsForm()
	{
		$id = $this->params['id'];
		$lang = isset($this->params['tab']) ? $this->params['tab'] : 'czTab';
		$this->editPageStructureMutations->setId($id);

		$form = $this->editPageStructureMutations;
		$form = $form->create($lang);
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Stránka byla uložena.', 'success');
			$form->getPresenter()->redirect('this', ['id' => $this->params['id']]);
		};
		return $form;
	}


	/**
	 * AddNewsImages form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddStructureImagesForm()
	{
		$id = $this->params['id'];
		$this->addStructureImages->setId($id);

		$form = $this->addStructureImages->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Obrázky byly nahrány.', 'success');
			$form->getPresenter()->redirect('this', ['id' => $this->params['id'], 'tab' => 'galerie']);
		};
		return $form;
	}


	/**
	 * AddNewsImages form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddStructurePrilohyForm()
	{
		$id = $this->params['id'];
		$this->addStructurePrilohy->setId($id);

		$form = $this->addStructurePrilohy->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Přílohy byly nahrány.', 'success');
			$form->getPresenter()->redirect('this', ['id' => $this->params['id'], 'tab' => 'prilohy']);
		};
		return $form;
	}

}
