<?php

namespace App\AdminModule\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class SlidersPresenter extends BasePresenter
{

	/** @var \App\Model\MenuBuilder @inject */
	public $menuBuilder;

	/** @var \App\Model\Slider @inject */
	public $slider;

	/** @var \App\Forms\AddSliderForm @inject */
	public $addSlider;

	/** @var \App\Forms\RenameSliderForm @inject */
	public $renameSlider;

	/** @var \App\Forms\EditSlideForm @inject */
	public $editSlide;

	/** @var \App\Forms\AddSlideForm @inject */
	public $addSlide;

	public $editItemId;


	public function actionDefault()
	{
		$this->template->menu = $this->slider->getSliderDefault();
	}

	public function actionEdit($id)
	{
		$this->editItemId = $id;
		$this->template->editItemId = $id;

		$this->template->parentItem = $this->slider->getSliderByKey($id);
		$this->template->menu = $this->slider->getSliderEdit($id);
	}

	public function actionDelete($id)
	{
		if (!$id) {
			$this->flashMessage('Vyberte platný slider');
			$this->redirect('Sliders:default');
		}

		$status = $this->slider->deleteSlider($id);

		if ((bool)$status === TRUE) {
			$this->flashMessage('Slider byl úspěšně smazán', 'success');
			$this->redirect('Sliders:');
		} else {
			$this->flashMessage('Slider nebyl úspěšně smazán, zkuste to prosím znovu', 'warning');
			$this->redirect('Sliders:');
		}
	}

	public function actionDeleteItem($id,$sliderId)
	{
		if (!$id || !$sliderId) {
			$this->flashMessage('Vyberte platný slide');
			$this->redirect('Sliders:default');
		}

		$this->slider->path = $this->presets->getPresets()->path;
		$status = $this->slider->deleteSlide($id);

		if ((bool)$status === true) {
			$this->flashMessage('Slide byl úspěšně smazán', 'success');
			$this->redirect('Sliders:edit', array('id' => $sliderId));
		} else {
			$this->flashMessage('Slide nebyl úspěšně smazán, zkuste to prosím znovu', 'warning');
			$this->redirect('Sliders:edit', array('id' => $sliderId));
		}
	}

	public function handleSaveStructure($jsonString)
	{
		if ($this->isAjax()) {
			$data = json_decode( $jsonString );
			$this->menuBuilder->table_name = 'slider';
			$out = $this->menuBuilder->saveStructure($data,$this->editItemId);
			$this->sendResponse(new Nette\Application\Responses\JsonResponse(['complete' => $out]));
		}

	}

	/* factories **************************************************/


	/**
	 * AddPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddSliderForm()
	{
		$form = $this->addSlider->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Slider byl přidán.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

	/**
	 * AddPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddSlideForm()
	{
		$id = $this->params['id'];
		$this->addSlide->setId($id);
		$this->addSlide->setPath($this->presets->getPresets()->path);

		$form = $this->addSlide->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Slider byl přidán.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

	/**
	 * AddPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentRenameSliderForm()
	{

		$form = $this->renameSlider->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Slider přejmenován.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

	/**
	 * AddPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentEditSlideForm()
	{

		$form = $this->editSlide->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Slide upraven.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}



}
