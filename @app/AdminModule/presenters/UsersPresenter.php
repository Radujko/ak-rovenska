<?php

namespace App\AdminModule\Presenters;

use Nette,
	App\Model,
	Nette\Security,
	Nette\Security\User,
 	App\Forms\NewOfferForm;


/**
 * Users presenter.
 */
class UsersPresenter extends BasePresenter
{

	/** @var \App\Model\Users @inject */
	public $users;

	/** @var \App\Model\Acl @inject */
	public $acl;

	/** @var \App\Forms\AddUserForm @inject */
	public $addUser;

	/** @var \App\Forms\EditUserForm @inject */
	public $editUser;


	public function startup()
	{
		parent::startup();

		if (!$this->user->isLoggedIn()) {
			$this->flashMessage('Pro přístup musíte být přihlášen.', 'error');
			$this->redirect(':Sign:in', array('backlink' => $this->storeRequest()));
		}

		//dump($this->acl->getId());exit;



	}

	public function actionDefault()
	{
		$this->template->users = $this->users->getUsers();

		
		foreach ($this->user->roles as $role) {
			$this->template->signedUserRole = $role;
		}

		
		/*
		if ($this->user->isInRole('editor')) {
			dump('true');
		} else {
			dump('false');
		}*/

	}

	public function actionDelete($id)
	{
		if (!$id) {
			$this->flashMessage('Vyberte platnou stánku');
			$this->redirect('Structure:default');
		}

		
		$status = $this->users->deleteUser($id);

		if ((bool)$status === true) {
			$this->flashMessage('Uživatel byl úspěšně smazán', 'success');
			$this->redirect('Users:default');
		} else {
			$this->flashMessage('Uživatel nebyl smazán, zkuste to prosím znovu', 'warning');
			$this->redirect('Users:default');
		}
		
	}



	/* factories **************************************************/


	/**
	 * AddPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddUserForm()
	{
		$form = $this->addUser->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Uživatel byl vytvořen.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

	/**
	 * EditPageStructure form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentEditUserForm()
	{
		$form = $this->editUser->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Uživatel byl změněn.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

}
