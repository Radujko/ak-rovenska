<?php

namespace App\AdminModule\Presenters;

use Nette,
	App\Model;
use App\Forms\NewOfferForm;


/**
 * Homepage presenter.
 */
class DashboardPresenter extends BasePresenter
{

	public function startup()
	{
		parent::startup();

		if (!$this->user->isLoggedIn()) {
			$this->flashMessage('Pro přístup musíte být přihlášen.', 'error');
			$this->redirect(':Sign:in', array('backlink' => $this->storeRequest()));
		}

	}





}
