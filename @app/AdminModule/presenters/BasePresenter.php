<?php

namespace App\AdminModule\Presenters;

use Nette,
	Latte,
	App\Model\SimpleTranslator,
	App;
	


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends App\Presenters\BasePresenter
{

	public function startup()
	{
		parent::startup();

		if (!$this->user->isLoggedIn()) {
			$this->flashMessage('Pro přístup musíte být přihlášen.', 'error');
			$this->redirect(':Sign:in', array('backlink' => $this->storeRequest()));
		}

		$this->template->news = $this->news->fullNews();
		$this->template->presets = $this->presets->getPresets();
		$this->template->lang = $this->lang;
		$this->translator = new SimpleTranslator($this->lang);
		$this->template->setTranslator($this->translator);

	}

	protected function createTemplate($class = NULL)
	{
		$tpl = parent::createTemplate($class);
		
		// Shortcut
		$presenter = $this;
		$tpl->current = function($pattern) use ($presenter) {
			foreach ((array) $pattern as $opt) {
				if ($presenter->isLinkCurrent($opt)) {
					return TRUE;
				}
			}
			return FALSE;
		};

		$presets = $this->presets->getPresets();
		$tpl->addFilter('version', function($path) use ($tpl, $presets) {
			$relPath = trim(Nette\Utils\Strings::replace($path, "#^{$tpl->basePath}#"), '/');
			$filePath = "$presets->wwwDir/$relPath";
			if (file_exists($filePath)) {
				return $path . '?' . filemtime($filePath);
			}
			return $path;
		});

		return $tpl;
	}

	public function actionDefault()
	{

	}





		
}
