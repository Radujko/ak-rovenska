<?php

namespace App\AdminModule\Presenters;

use Nette,
	Latte,
	App;


class NewsPresenter extends BasePresenter
{

	/** @var \App\Model\News @inject */
	public $news;

	/** @var \App\Forms\AddNewsForm @inject */
	public $addNews;

	/** @var \App\Forms\AddNewsImagesForm @inject */
	public $addNewsImages;

	/** @var \App\Forms\EditNewsForm @inject */
	public $editNews;


	public function actionDefault()
	{
		$this->news->setPath($this->presets->getPresets()->path);
		$this->news->setSizes($this->presets->getPresets()->sizes);

		$this->template->news = $this->news->getNews(true);
		$this->template->fullNews = $this->news->fullNews();
	}

	public function actionEdit($id = null, $tab)
	{
		$this->news->setPath($this->presets->getPresets()->path);
		$this->news->setSizes($this->presets->getPresets()->sizes);

		$this->template->images = $this->news->getImages($id);

		if ($this->isAjax()) {
			$tab = $this->getHttpRequest()->getPost('tab');
			$this->redirect('this', array('tab' => $tab));
		}

		if(isset($tab)) {
			$this->template->tab = $tab;	
		} else {
			$this->template->tab = 'czText';
		}
	}

	public function actionAdd($tab)
	{
		if(isset($tab)) {
			$this->template->tab = $tab;	
		} else {
			$this->template->tab = 'czText';
		}

		if ($this->isAjax()) {
			$tab = $this->getHttpRequest()->getPost('tab');
			$this->redirect('this', array('tab' => $tab));
		}
	}



	/* handlers ****************************************************/


	public function handleDeactivate($news_id, $state = 0)
	{
		$status = $this->news->deactivateNews($news_id, $state);
		if ($status) {
			$this->flashMessage('Novinka byla deaktivována.');
		} else {
			$this->flashMessage('Novinka nebyla deaktivována, prosím zkuste to znovu.', 'warning');
		}
		$this->redirect('this');
	}


	public function handleSetPrimary($id, $image_id, $tab)
	{
		$status = $this->news->primaryImage($image_id);
		$this->template->images = $this->news->getImages($id);
		$this->redrawControl();
	}


	public function handleDeleteimage($id, $image_id)
	{

		$status = $this->news->deleteImage($image_id);
		$this->template->images = $this->news->getImages($id);

		if ($status) {
			$this->flashMessage('Obrázek byl odstraněn');
		} else {
			$this->flashMessage('Obrázek nebyl odstraněn, prosím zkuste to znovu.', 'warning');
		}

		$this->redrawControl();
	}

	public function actionDelete($id)
	{
		if (!$id) {
			$this->flashMessage('Vyberte platnou novinku');
			$this->redirect('News:default');
		}

		$status = $this->news->deleteNew($id);

		if ((bool)$status === true) {
			$this->flashMessage('Novinka byla úspěšně smazána', 'success');
			$this->redirect('News:default');
		} else {
			$this->flashMessage('Novinka nebyla úspěšně smazána, zkuste to prosím znovu', 'warning');
			$this->redirect('News:default');
		}
	}



	/* factories **************************************************/


	/**
	 * AddNews form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddNewsForm()
	{
		$this->addNews->setPath($this->presets->getPresets()->path);
		$lang = isset($this->params['tab']) ? $this->params['tab'] : 'czTab';
		
		$form = $this->addNews->create($lang);
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Aktualita byla vložena.', 'success');
			$form->getPresenter()->redirect('News:');
		};
		return $form;
	}


	/**
	 * AddNews form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentEditNewsForm()
	{
		$id = $this->params['id'];
		$lang = isset($this->params['tab']) ? $this->params['tab'] : 'czTab';
		$this->editNews->setId($id);
		$this->editNews->setPath($this->presets->getPresets()->path);
		$form = $this->editNews->create($lang);
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Aktualita byla upravena.', 'success');
			$form->getPresenter()->redirect('this', ['id' => $this->params['id']]);
		};
		return $form;
	}


	/**
	 * AddNewsImages form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddNewsImagesForm()
	{
		$id = $this->params['id'];
		$this->addNewsImages->setId($id);

		$form = $this->addNewsImages->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Obrázky byly nahrány.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

}
