<?php

namespace App\AdminModule\Presenters;

use Nette,
	Latte,
	App;


class GalleryPresenter extends BasePresenter
{

	/** @var \App\Model\Gallery @inject */
	public $gallery;

	/** @var \App\Forms\AddGalleryForm @inject */
	public $addGallery;

	/** @var \App\Forms\AddGalleryImagesForm @inject */
	public $addGalleryImage;

	/** @var \App\Forms\EditGalleryForm @inject */
	public $editGallery;


	public function actionDefault()
	{
		$this->gallery->setPath($this->presets->getPresets()->path);
		$this->gallery->setSizes($this->presets->getPresets()->sizes);

		$this->template->pages = $this->gallery->getPages(true);
		$this->template->fullPages = $this->gallery->fullPages();
	}

	public function actionEdit($id = null, $tab)
	{
		$this->gallery->setPath($this->presets->getPresets()->path);
		$this->gallery->setSizes($this->presets->getPresets()->sizes);

		$this->template->images = $this->gallery->getImages($id);

		if ($this->isAjax()) {
			$tab = $this->getHttpRequest()->getPost('tab');
			$this->redirect('this', array('tab' => $tab));
		}

		if(isset($tab)) {
			$this->template->tab = $tab;
		} else {
			$this->template->tab = 'czText';
		}
	}

	public function actionAdd($tab)
	{
		if(isset($tab)) {
			$this->template->tab = $tab;
		} else {
			$this->template->tab = 'czText';
		}

		if ($this->isAjax()) {
			$tab = $this->getHttpRequest()->getPost('tab');
			$this->redirect('this', array('tab' => $tab));
		}
	}



	/* handlers ****************************************************/


	public function handleDeactivate($gellery_id, $state = 0)
	{
		$status = $this->gallery->deactivateNews($gellery_id, $state);
		if ($status) {
			$this->flashMessage('Novinka byla deaktivována.');
		} else {
			$this->flashMessage('Novinka nebyla deaktivována, prosím zkuste to znovu.', 'warning');
		}
		$this->redirect('this');
	}


	public function handleSetPrimary($id, $image_id, $tab)
	{
		$status = $this->gallery->primaryImage($image_id);
		$this->template->images = $this->gallery->getImages($id);
		$this->redrawControl();
	}


	public function handleDeleteimage($id, $image_id)
	{

		$status = $this->gallery->deleteImage($image_id);
		$this->template->images = $this->gallery->getImages($id);

		if ($status) {
			$this->flashMessage('Obrázek byl odstraněn');
		} else {
			$this->flashMessage('Obrázek nebyl odstraněn, prosím zkuste to znovu.', 'warning');
		}

		$this->redrawControl();
	}



	/* factories **************************************************/


	/**
	 * AddNews form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddGalleryForm()
	{
		$this->addGallery->setPath($this->presets->getPresets()->path);
		$lang = isset($this->params['tab']) ? $this->params['tab'] : 'czTab';

		$form = $this->addGallery->create($lang);
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Galerie vytvořena.', 'success');
			$form->getPresenter()->redirect('Gallery:');
		};
		return $form;
	}


	/**
	 * AddNews form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentEditGalleryForm()
	{
		$id = $this->params['id'];
		$lang = isset($this->params['tab']) ? $this->params['tab'] : 'czTab';
		$this->editGallery->setId($id);
		$this->editGallery->setPath($this->presets->getPresets()->path);
		$form = $this->editGallery->create($lang);
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Galerie byla upravena.', 'success');
			$form->getPresenter()->redirect('this', ['id' => $this->params['id']]);
		};
		return $form;
	}


	/**
	 * AddNewsImages form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentAddGalleryImagesForm()
	{
		$id = $this->params['id'];
		$this->addGalleryImage->setId($id);

		$form = $this->addGalleryImage->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->flashMessage('Obrázky byly nahrány.', 'success');
			$form->getPresenter()->redirect('this');
		};
		return $form;
	}

}
