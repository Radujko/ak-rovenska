<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;
use Nette\Security\Passwords;


/**
 * Users management.
 * user roles from higgest - admin, editor, moderator, member, user, quest
 */
class Users extends Nette\Object
{

	const 
		TABLE = 'users',
		COLUMN_ID = 'id',
		COLUMN_NAME = 'username',
		COLUMN_NAME_REAL = 'name',
		COLUMN_EMAIL = 'email',
		COLUMN_PASSWORD_HASH = 'password',
		COLUMN_CREATED = 'created',
		COLUMN_ROLE = 'role',
		COLUMN_STATUS = 'status';
		

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;


	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}

	public function getUsers()
	{
		$data = $this->table(self::TABLE)->order('created ASC')->fetchAll();

		return $data;
	}


	/**
	 * Adds new user.
	 * @param  string
	 * @param  string
	 * @return void
	 */
	public function addUser($data)
	{
		try {
			$this->table(self::TABLE)->insert(array(
				self::COLUMN_NAME => $data->username,
				self::COLUMN_NAME_REAL => $data->name,
				self::COLUMN_EMAIL => $data->email,
				self::COLUMN_PASSWORD_HASH => Passwords::hash($data->password),
				self::COLUMN_CREATED => $data->created,
				self::COLUMN_ROLE => $data->role,
				self::COLUMN_STATUS => $data->status
			));
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}

	
	public function updateUser($key, $data)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->update($data);

		return $status;
	}

	public function deleteUser($key)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->delete();

		return $status;
	}


	/* special function to get table names with prefix */


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}


}

