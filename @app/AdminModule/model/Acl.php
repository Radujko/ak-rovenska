<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


/**
 * Users management.
 * user roles from higgest - admin, editor, moderator, member, user, quest
 */
class Acl extends Nette\Object
{

	const 
		TABLE = 'users';


	private $admin = 'admin';
	private $editor = 'editor';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $u;

	/** @var App\Model\Presets */
	public $presets;


	public function __construct(Nette\Database\Context $database, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->presets = $presets->getPresets();
	}


	public function allowed($minLevel = 'editor')
	{
		$role = $this->u->getRoles()[0];
		dump($this->u);exit;
		return in_array($role, $this->$minLevel);

	}
}

