<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


class MenuBuilder extends Nette\Object
{
    const
	    TABLE = 'structure',
	    TABLE_DATA = 'structure_data';

	public $table_name = self::TABLE;

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;

	public $menuData;

	public $defaultParentId = 0;

	public $totalChildrens;

	private $lastSaveLi;

	private $menuItemToSave = array();

    /**
     * @param Nette\Database\Connection $database
     */
	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}

	private function insertItem($parent_id, $id, $position) {

		$this->menuItemToSave[] = array(
			'parent_id' => $parent_id,
			'id' => $id,
			'position' => $position
		);
	}

	private function prepareStructureToSaveSubs($data)
	{
		$c = 0;
		$parent_id = $this->lastSaveLi;
		foreach ($data[0] as $i) { $c++;
			$this->insertItem($parent_id, $i->id, $c);

			if (count($i->children[0]) !== 0) {
				$this->lastSaveLi = $i->id;
				$this->prepareStructureToSaveSubs($i->children);
			}
		}
	}

	private function prepareStructureToSave($data)
	{
		$c = 0;
		foreach ($data[0] as $i) { $c++;
			$this->insertItem($this->defaultParentId, $i->id, $c);

			if (isset($i->children)) {
				if (count($i->children[0]) !== 0) {
					$this->lastSaveLi = $i->id;
					$this->prepareStructureToSaveSubs($i->children);
				}
			}
		}

		return $this->menuItemToSave;
	}

	public function saveStructure($data, $defaultParentId = 1)
	{
		$this->defaultParentId = $defaultParentId;
		$preparedData = $this->prepareStructureToSave($data);
		foreach ($preparedData as $d) {
			$id = $d['id'];
			unset($d['id']);
			$out = $this->table($this->table_name)->where('id', $id)->update($d);
		}

		return $out;
	}

	public function getTotalChildrensSubs($parent_id = 0)
	{
		$this->totalChildrens++;

		$data = $this->table(self::TABLE)->select('id')->where('parent_id',$parent_id)->fetchAll();

		if (empty($data) === false) {
			foreach ($data as $i) {
				$this->getTotalChildrensSubs($i->id);
			}
		}
	}

	public function getTotalChildrens($parent_id = 0)
	{
		$this->totalChildrens = 0;
		$data = $this->table(self::TABLE)->select('id')->where('parent_id',$parent_id)->fetchAll();

		foreach ($data as $i) {
			$this->getTotalChildrensSubs($i->id);
		}

		return $this->totalChildrens;
	}

	public function getArraySubs($parent_id = 0)
	{
		$data = $this->table(self::TABLE)->where('parent_id',$parent_id)->order('parent_id, position ASC')->fetchAll();
		$menuData = array();

		if (empty($data) === false) {
			foreach ($data as $i) {
				$subs = $this->getArraySubs($i->id);
				$n = array(
					'id' => $i->id,
					'name' => $i->name,
					'status' => $i->status,
					'homepage' => $i->homepage,
					'parent_id' => $i->parent_id,
					'url' => $this->getUrl($i->id)
				);
				if (isset($i->itemId)) {
					$n += array('itemId' => $i->itemId);
				}
				if ($subs) {
					$n += array('subs' => $subs);
				}
				$menuData[] = (object)$n;
			}

			return $menuData;
		}
	}

	public function getArray($parent_id = 0, $allow_subs = true)
	{
		$this->menuData = array();
		$data = $this->table(self::TABLE)->where('parent_id',$parent_id)->order('parent_id, position ASC')->fetchAll();

		foreach ($data as $i) {
			$subs = $this->getArraySubs($i->id);
			$n = array(
				'id' => $i->id,
				'name' => $i->name,
				'status' => $i->status,
				'homepage' => $i->homepage,
				'parent_id' => $i->parent_id,
				'url' => $this->getUrl($i->id),
				'count' => $this->getTotalChildrens($i->id)
			);
			if (isset($i->itemId)) {
				$n += array('itemId' => $i->itemId);
			}
			if ($subs && $allow_subs) {
				$n += array('subs' => $subs);
			}
			$this->menuData[] = (object)$n;
		}

		return $this->menuData;
	}

	public function getUrl($key)
	{
		$url = $this->table(self::TABLE_DATA)->where('itemId',$key)->fetch();
		if (isset($url->url)) {
			$url = $url->url;
		}

		return $url;
	}

	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}


}

 
?>