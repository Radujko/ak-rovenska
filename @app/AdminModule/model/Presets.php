<?php
namespace App\Model;

use Nette;


/**
 * return presets as object
 */
class Presets extends Nette\Object
{

	public $defaults;


	public function __construct()
	{
		//return self::getPresets();		
	}


	public function getPresets()
	{
		$presets = new \stdClass();

		foreach ($this->defaults as $key => $value) {
			$presets->$key = $value;
		}

		return $presets;
	}


	public function setDefaults($defaults)
	{
		$this->defaults = $defaults;
	}

	
}

