<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


class Structure extends Nette\Object
{
	const
		TABLE = 'structure',
		TABLE_PRILOHY = 'structure_prilohy',
		TABLE_IMAGES = 'structure_images',
		TABLE_DATA = 'structure_data',
		TABLE_MENU = 'menu';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;

	public $path;
	public $sizes;

	public $unsortedIdItem = 2;
 
    /**
     * @param Nette\Database\Connection $database
     */
	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}

	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}

	public function getPageByKey($key)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->fetch();

		return $status;
	}

	public function getPageDataByKey($key)
	{
		$status = $this->table(self::TABLE_DATA)->where('itemId', $key)->fetch();

		return $status;
	}

	public function getPrilohy($id)
	{
		return $this->table(self::TABLE_PRILOHY)->where('track_id', $id);
	}

	public function getImages($id)
	{
		return $this->table(self::TABLE_IMAGES)->where('track_id', $id);
	}

	public function primaryImage($image_id)
	{
		$image = $this->table(self::TABLE_IMAGES)->where('id', $image_id)->fetch();
		$images = self::getImages($image->track_id);
		foreach ($images as $i) {
			$i->update(array('cover' => 0));
		}
		$image->update(array('cover' => 1));
		return true;
	}

	public function deleteImage($id)
	{
		$files = [];
		$image = $this->table(self::TABLE_IMAGES)->get($id);
		if (!$image) {
			return FALSE;
		}

		$mainImg = $this->path . 's' . $image->track_id . '/' . $image->name;
		$dir = $this->path . 's' . $image->track_id . '/';

		if (file_exists($mainImg)) {
			$files[] = $mainImg;
		}

		foreach ($this->sizes as $key => $value) {
			$i = $this->path . 's' . $image->track_id . '/' . $key . '_' . $image->name;
			if (file_exists($i)) {
				$files[] = $i;
			}
		}

		foreach ($files as $f) {
			unlink($f);
		}

		$image->delete();

		$this->checkDirEmptyToDelete($dir);

		return TRUE;
	}

	public function deletePriloha($id)
	{
		$priloha = $this->table(self::TABLE_PRILOHY)->get($id);
		if (!$priloha) {
			return FALSE;
		}

		$file = $this->path . 's' . $priloha->track_id . '/' . $priloha->name;
		$dir =  $this->path . 's' . $priloha->track_id . '/';

		if (file_exists($file)) {
			unlink($file);
		}

		$priloha->delete();

		$this->checkDirEmptyToDelete($dir);

		return TRUE;
	}

	public function checkDirEmptyToDelete($dir)
	{
		if ($this->is_dir_empty($dir)) {
			return rmdir($dir);
		}
	}

	public function is_dir_empty($dir)
	{
		if (!is_readable($dir)) return NULL;
		$handle = opendir($dir);
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				return FALSE;
			}
		}
		return TRUE;
	}

	public function addImage($id, $name)
	{
		$this->table(self::TABLE_IMAGES)->insert(array(
			'track_id' => $id,
			'name' => $name,
			'add_by' => $this->user_id,
			'add_time' => date('Y-m-d H:i:s'),
		));
	}

	public function addPrilohy($id, $name, $p_name, $ext, $size)
	{
		$this->table(self::TABLE_PRILOHY)->insert(array(
			'track_id' => $id,
			'size' => $size,
			'name' => $name,
			'p_name' => $p_name,
			'ext' => $ext,
			'add_by' => $this->user_id,
			'add_time' => date('Y-m-d H:i:s'),
		));
	}

	public function addPage($data)
	{
		$url = Strings::webalize($data->name);
		unset($data->url);
		$data->parent_id = $this->unsortedIdItem;

		$status1 = $this->table(self::TABLE)->insert($data);
		$status2 = $this->table(self::TABLE_DATA)->insert(array('itemId' => $status1, 'menuText_cz' => $data->name, 'title_cz' => $data->name, 'url' => $url));

		return $status1;
	}

	public function isUsedInMenu($key)
	{
		$status = $this->table(self::TABLE_MENU)->where('itemId', $key)->fetch();

		return $status;
	}

	public function deletePage($key)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->delete();
		$status = $this->table(self::TABLE_DATA)->where('itemId', $key)->delete();

		return $status;
	}

	public function updatePage($key, $data)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->update($data);

		return $status;
	}

	public function updateAllPages($data)
	{
		$status = $this->table(self::TABLE)->update($data);

		return $status;
	}

	public function updatePageData($key, $data)
	{
		$status = $this->table(self::TABLE_DATA)->where('itemId', $key)->update($data);

		return $status;
	}


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}

}

 
?>