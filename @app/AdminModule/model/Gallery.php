<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


/**
 * Users management.
 * user roles from higgest - admin, editor, moderator, member, user, quest
 */
class Gallery extends Nette\Object
{

	const
		TABLE = 'gallery',
		IMAGES = 'gallery_images';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;

	/** @var Nette\Http\Request */
	public $container;

	public $path;
	public $sizes;

	public $lang = 'cz';
	public $lang_url = '';

	public $baseUrl;

	public $galleryByFilter = false;
	public $filterParams = ['category_foto', 'category_video', 'category_kultura', 'category_sport'];
	public $filters = [];


	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets, Nette\Http\Request $container)
	{
		$this->container = $container;
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}

	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}

	public function get_youtube_thumb($hash, $type = 'hqdefault')
	{
		if ($type == 'default' || $type == 'hqdefault' || $type == 'mqdefault' || $type == 'sddefault' || $type == 'maxresdefault'){

			$thumb_link = 'http://img.youtube.com/vi/'.$hash.'/'.$type.'.jpg';
		}
		else {
			return NULL;
		}

		return $thumb_link;
	}

	public function getFilters()
	{
		if ($this->galleryByFilter) {

			foreach ($this->filterParams as $p) {
				$state = $this->container->getCookie($p);
				if ($state == null || $state == 'true') {
					$this->filters[$p] = TRUE;
				}
			}

		}
		return $this->filters;
	}

	public function addPage($values)
	{
		$values->inserted_by = $this->user_id;
		$values->inserted_date = date('Y-m-d H:i:s');

		$rows = $this->table(self::TABLE)->insert($values);

		return $rows;
	}


	public function getPages($all = false, $paginator = false)
	{
		$filters = $this->getFilters();

		if (count($filters) == 0 && $this->galleryByFilter) {
			return FALSE;
		}

		if ($all) {

			return $this->table(self::TABLE)->whereOr($filters)->order('date DESC');
		} else if ($paginator) {
			return $this->table(self::TABLE)->whereOr($filters)->where('visible', 1)->order('date DESC')->limit($paginator->getLength(), $paginator->getOffset());
		} else {
			return $this->table(self::TABLE)->whereOr($filters)->where('visible', 1)->order('top DESC')->order('date DESC');
		}
	}

	public function countMaxPages($all = false)
	{
		$filters = $this->getFilters();

		if (count($filters) == 0 && $this->galleryByFilter) {
			return FALSE;
		}

		if ($all) {
			return $this->table(self::TABLE)->whereOr($filters)->count();
		} else {
			return $this->table(self::TABLE)->whereOr($filters)->where('visible', 1)->count();
		}
	}

	public function countImages($key)
	{
		return $this->table(self::IMAGES)->where('track_id', $key)->count();
	}


	public function fullPages($limit = false, $category = false, $top = true, $paginator = false)
	{
		$pages = self::getPages(false, $paginator);
		$return = array();
		$images = array();

		if (!$pages) {
			return;
		}

		if ($top) {
			$pages->where('top', 1);
		}

		if ($limit) $pages = $pages->limit($limit);

		if ($pages->count() == 0) return array();

		//dump($this->presets->prefix.'news_id');exit;

		foreach ($pages as $p) {
			$id = $p->id;
			$image = false;
			$i = $this->table(self::IMAGES)->where('track_id', $id)->order('cover DESC')->order('sort')->limit(1);
			if ($i->count() > 0) $image = $i->fetch()->name;

			$r = new \StdClass();
			$r->id = $p->id;
			$r->date = $p->date;

			$r->headline_cz = $p->headline_cz;
			$r->headline_de = $p->headline_de;
			$r->headline_ru = $p->headline_ru;
			$r->headline_en = $p->headline_en;

			$r->text_cz = $p->text_cz;
			$r->text_de = $p->text_de;
			$r->text_en = $p->text_en;
			$r->text_ru = $p->text_ru;

			$r->video = $p->video;
			$r->video_thumb = $this->get_youtube_thumb($r->video);

			$r->visible = $p->visible;

			$r->images = $this->countImages($p->id);

			$r->images = $image;
			$return[] = $r;
		}

		return $return;

	}


	public function getFullPage($id)
	{
		$page = self::getPage($id,true);

		if ($page->count() > 0) {
			$page = $page->fetch();
		} else {
			return false;
		}

		$id = $page->id;
		$images = $this->table(self::IMAGES)->where('track_id', $page->id)->order('add_time DESC')->fetchAll();

		$r = new \StdClass();
		$r->id = $page->id;
		$r->date = $page->date;

		$r->headline_cz = $page->headline_cz;
		$r->headline_de = $page->headline_de;
		$r->headline_ru = $page->headline_ru;
		$r->headline_en = $page->headline_en;

		$r->text_cz = $page->text_cz;
		$r->text_de = $page->text_de;
		$r->text_en = $page->text_en;
		$r->text_ru = $page->text_ru;

		$r->video = $page->video;
		$r->video_thumb = $this->get_youtube_thumb($r->video);

		$r->visible = $page->visible;

		$r->images = $this->countImages($page->id);

		$r->images = $images;
		return $r;
	}


	public function getNextId()
	{
		$rows = $this->table(self::TABLE)->order('id DESC')->limit(1);
		if ($rows->count() > 0) {
			return $rows->fetch()->id + 1;
		} else {
			return 1;
		}

	}


	public function deactivatePage($id, $state)
	{
		return $this->table(self::TABLE)->where('id', $id)->update(array('visible' => $state));
	}


	public function getPage($id, $check = false)
	{
		if ($check) {
			return $this->table(self::TABLE)->where('id', $id);
		} else {
			return $this->table(self::TABLE)->where('id', $id)->fetch();
		}

	}


	public function editPage($values)
	{
		$id = $values->id;
		unset($values->id);
		$values->modified_by = $this->user_id;
		$values->modified_date = date('Y-m-d H:i:s');

		if (!isset($values->image)) $values->image = '0/0.jpg';

		return $this->table(self::TABLE)->where('id', $id)->update($values);

	}

	public function addImage($id, $name)
	{
		$this->table(self::IMAGES)->insert(array(
			'track_id' => $id,
			'name' => $name,
			'add_by' => $this->user_id,
			'add_time' => date('Y-m-d H:i:s'),
		));
	}


	public function primaryImage($image_id)
	{
		$image = $this->table(self::IMAGES)->where('id', $image_id)->fetch();
		$images = self::getImages($image->track_id);
		foreach ($images as $i) {
			$i->update(array('cover' => 0));
		}
		$image->update(array('cover' => 1));
		return true;
	}

	public function getImages($id)
	{
		return $this->table(self::IMAGES)->where('track_id', $id);
	}


	public function deleteImage($id)
	{
		$files = [];
		$image = $this->table(self::IMAGES)->get($id);
		if (!$image) {
			return FALSE;
		}

		$dir = $this->path . 'g' . $image->track_id;
		$mainImg = $dir . '/' . $image->name;

		if (file_exists($mainImg)) {
			$files[] = $mainImg;
		}

		foreach ($this->sizes as $key => $value) {
			$i = $dir . '/' . $key . '_' . $image->name;
			if (file_exists($i)) {
				$files[] = $i;
			}
		}

		foreach ($files as $f) {
			unlink($f);
		}

		$image->delete();

		$this->checkDirEmptyToDelete($dir);

		return TRUE;
	}

	public function checkDirEmptyToDelete($dir)
	{
		if ($this->is_dir_empty($dir)) {
			return rmdir($dir);
		}
	}

	public function is_dir_empty($dir)
	{
		if (!is_readable($dir)) return NULL;
		$handle = opendir($dir);
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				return FALSE;
			}
		}
		return TRUE;
	}


	/* special function to get table names with prefix */


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}


}

