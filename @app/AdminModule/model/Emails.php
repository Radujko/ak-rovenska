<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;
use Nette\Mail\Message;
use	Nette\Mail\SendmailMailer;
use Nette\Latte\Engine;


/**
 * Users management.
 * user roles from higgest - admin, editor, moderator, member, user, quest
 */
class Emails extends Nette\Object
{

	const 
		TABLE = 'contact';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;


	public function __construct(Nette\Database\Context $database, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->presets = $presets->getPresets();
	}

	public function sendContact($values)
	{
		$email = $values->email;
		$isEmail = false;
		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) $isEmail = true;

		$latte = new \Latte\Engine;
		$params = array(
			'name' => $values->name,
			'email' => $values->email,
			'phone' => $values->phone,
			'message' => $values->message,
			'isEmail' => $isEmail,
		);

		$email = filter_var($email, FILTER_SANITIZE_EMAIL);

		$from = 'Skleton <no-reply@mediastudio.cz>';

		$studio = 'radujko@mediastudio.cz';

		// mail to carlaine
		$mail = new Message;
		$mail->setFrom($from)
			->addTo($studio)
			->setHtmlBody($latte->renderToString(__DIR__.'/../presenters/templates/components/emails/contactEmail.latte', $params));

		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$mail->addReplyTo($values->email, $values->name);
		}

		$mailer = new SendmailMailer;

//		$mailer = new Nette\Mail\SmtpMailer([
//			'host' => 'smtp.gmail.com',
//			'username' => 'franta@gmail.com',
//			'password' => '*****',
//			'secure' => 'ssl',
//		]);

		$mailer->send($mail);
	}


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}



}

