<?php

namespace App\Model;

use Nette;


/**
 * Simple translator.
 */
class SimpleTranslator implements Nette\Localization\ITranslator
{
	const SOURCE_FILE_PATH = 'translations.neon'; // relative to __DIR__

	/** @var string */
	private $lang;

	/** @var array */
	private static $translations;


	public function __construct($lang = 'cz')
	{
		$this->lang = $lang;

		$sourceFilePath = __DIR__ . '/' . self::SOURCE_FILE_PATH;
		if (!is_readable($sourceFilePath)) {
			throw new Nette\FileNotFoundException("File '$sourceFilePath' is not readable.");
		}
		
		self::$translations = Nette\Utils\Neon::decode(file_get_contents($sourceFilePath));
	}


	/**
	 * @param  string
	 * @param  int    plural count
	 * @return string
	 */
	public function translate($str, $count = NULL)
	{
		if (isset(self::$translations[$str][$this->lang])) {
			return self::$translations[$str][$this->lang];
		}

		if (isset(self::$translations[$this->lang][$str])) {
			return $translations[$this->lang][$str];
		}

		return $str;
	}
}
