<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


class Menu extends Nette\Object
{
    const
	    TABLE = 'menu',
	    TABLE_STRUCTURE = 'structure',
		TABLE_STRUCTURE_DATA = 'structure_data';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;

	public $menuData;

	public $totalChildrens;
 
    /**
     * @param Nette\Database\Connection $database
     */
	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}

	public function getMenuEditSubs($parent_id = 0)
	{
		$data = $this->table(self::TABLE)->where('parent_id',$parent_id)->order('parent_id, position ASC')->fetchAll();
		$menuData = array();

		if (empty($data) === false) {
			foreach ($data as $i) {
				$subs = $this->getMenuEditSubs($i->id);
				$n = array(
					'id' => $i->id,
					'status' => $i->status,
					'itemId' => $i->itemId,
					'parent_id' => $i->parent_id,
					'count' => $this->getTotalChildrens($i->id),
					'subs' => $subs
				);

				$itemStr = $this->getItemFromStructure($i->itemId);
				if (isset($itemStr->name)) {
					$n += array('name' => $itemStr->name);
				} else {
					$n += array('name' => '');
				}
				$n += array('statusStr' => $itemStr->status);

				$itemData = $this->getItemFromStructureData($i->itemId);
				if (isset($itemData->url)) {
					$n += array('url' => $itemData->url);
				} else {
					$n += array('url' => '');
				}

				$menuData[] = (object)$n;
			}

			return $menuData;
		}
	}

	public function getMenuEdit($parent_id = 0)
	{
		$this->menuData = array();
		$data = $this->table(self::TABLE)->where('parent_id',$parent_id)->order('parent_id, position ASC')->fetchAll();

		foreach ($data as $i) {
			$subs = $this->getMenuEditSubs($i->id);
			$n = array(
				'id' => $i->id,
				'status' => $i->status,
				'itemId' => $i->itemId,
				'parent_id' => $i->parent_id,
				'count' => $this->getTotalChildrens($i->id),
				'subs' => $subs
			);

			$itemStr = $this->getItemFromStructure($i->itemId);
			if (isset($itemStr->name)) {
				$n += array('name' => $itemStr->name);
			} else {
				$n += array('name' => '');
			}
			$n += array('statusStr' => $itemStr->status);

			$itemData = $this->getItemFromStructureData($i->itemId);
			if (isset($itemData->url)) {
				$n += array('url' => $itemData->url);
			} else {
				$n += array('url' => '');
			}

			$this->menuData[] = (object)$n;
		}

		return $this->menuData;
	}

	public function getMenuDefault()
	{
		$menuData = array();
		$data = $this->table(self::TABLE)->where('parent_id',0)->order('parent_id, position ASC')->fetchAll();

		foreach ($data as $i) {
			$n = array(
				'id' => $i->id,
				'name' => $i->name,
				'parent_id' => $i->parent_id,
				'count' => $this->getTotalChildrens($i->id)
			);

			$menuData[] = (object)$n;
		}

		return $menuData;
	}

	public function getTotalChildrensSubs($parent_id = 0)
	{
		$this->totalChildrens++;

		$data = $this->table(self::TABLE)->select('id')->where('parent_id',$parent_id)->fetchAll();

		if (empty($data) === false) {
			foreach ($data as $i) {
				$this->getTotalChildrensSubs($i->id);
			}
		}
	}

	public function getTotalChildrens($parent_id = 0)
	{
		$this->totalChildrens = 0;
		$data = $this->table(self::TABLE)->select('id')->where('parent_id',$parent_id)->fetchAll();

		foreach ($data as $i) {
			$this->getTotalChildrensSubs($i->id);
		}

		return $this->totalChildrens;
	}

	public function getItemFromStructure($key)
	{
		$item = $this->table(self::TABLE_STRUCTURE)->where('id',$key)->fetch();

		return $item;
	}

	public function getItemFromStructureData($key)
	{
		$item = $this->table(self::TABLE_STRUCTURE_DATA)->where('itemId',$key)->fetch();

		return $item;
	}

	public function getMenuByKey($key)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->fetch();

		return $status;
	}

	public function addMenu($data)
	{
		$status = $this->table(self::TABLE)->insert($data);

		return $status;
	}

	public function addMenuItem($data)
	{
		foreach ($data['pages'] as $p) {
			$status = $this->table(self::TABLE)->insert(array('itemId' => $p, 'parent_id' => $data['itemId']));
		}

		return $status;
	}

	public function deleteMenu($key)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->delete();

		return $status;
	}

	public function updateMenu($key, $data)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->update($data);

		return $status;
	}


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}

}

 
?>