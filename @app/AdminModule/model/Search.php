<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


class Search extends Nette\Object
{

	
	const 
		NEWS = 'news',
		GALLERY = 'gallery',
		STRUCTURE = 'structure_data';
		
	/** @var Nette\Database\Context */
	private $database;


	/** @var App\Model\Presets */
	public $presets;


	public function __construct(Nette\Database\Context $database, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->presets = $presets->getPresets();
	}


	public function searchArticles($needle)
	{

			$status = (object) [];

			$status->news = $this->database->query(
						"

							SELECT id, headline_cz, headline_en, headline_de, headline_ru, desc_cz, desc_en, desc_de, desc_ru,
							IFNULL(
								MATCH (headline_cz)
								AGAINST ('*\"".$needle."\"*')
								,
								MATCH (text_cz)
								AGAINST ('*\"".$needle."\"*')
							) AS Relevance

							FROM ".$this->presets->prefix . self::NEWS."
							WHERE (
									MATCH(headline_cz, headline_en, headline_de, headline_ru, text_cz, text_en, text_de, text_ru)
									AGAINST('*\"".$needle."\"*' IN BOOLEAN MODE)
							)

						"
					)
				    ->fetchAll();

			$status->gallery = $this->database->query(
						"

							SELECT id, headline_cz, headline_en, headline_de, headline_ru, text_cz, text_en, text_de, text_ru,
							IFNULL(
								MATCH (headline_cz)
								AGAINST ('*\"".$needle."\"*')
								,
								MATCH (text_cz)
								AGAINST ('*\"".$needle."\"*')
							) AS Relevance

							FROM ".$this->presets->prefix . self::GALLERY."
							WHERE (
									MATCH(headline_cz, headline_en, headline_de, headline_ru, text_cz, text_en, text_de, text_ru)
									AGAINST('*\"".$needle."\"*' IN BOOLEAN MODE)
							)

						"
					)
				    ->fetchAll();

			$status->structure = $this->database->query(
						"

							SELECT id, itemId, menuText_cz, menuText_en, menuText_de, menuText_ru, anotace_cz, anotace_en, anotace_de, anotace_ru,
							IFNULL(
								MATCH (menuText_cz)
								AGAINST ('*\"".$needle."\"*')
								,
								MATCH (text1_cz)
								AGAINST ('*\"".$needle."\"*')
							) AS Relevance

							FROM ".$this->presets->prefix . self::STRUCTURE."
							WHERE (
									MATCH(menuText_cz, menuText_en, menuText_de, menuText_ru, text1_cz, text1_en, text1_de, text1_ru)
									AGAINST('*\"".$needle."\"*' IN BOOLEAN MODE)
							)

						"
					)
				    ->fetchAll();

			


		return $status;

/*
	   

*/
	}

	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}





}

