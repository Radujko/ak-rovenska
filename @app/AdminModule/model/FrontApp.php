<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;
use Nette\Templating\FileTemplate;


/**
 * Users management.
 * user roles from higgest - admin, editor, moderator, member, user, quest
 */
class FrontApp extends Nette\Object
{

	const
		TABLE_STRUCTURE = 'structure',
		TABLE_STRUCTURE_DATA = 'structure_data',
		TABLE_MENU = 'menu',
		TABLE_SLIDER = 'slider';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;

	public $menuData;

	public $lang = 'cz';
	public $lang_url = '';

	public $baseUrl;


	/**
	 * @param Nette\Database\Connection $database
	 */
	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}


	/**
	 * ---------------
	 * --------------- Výpis menu
	 * ---------------
	 */
	public function getMenuSubs($parent_id)
	{
		$menuData = array();
		$data = $this->table(self::TABLE_MENU)->where(['parent_id' => $parent_id, 'status' => 1])->order('parent_id, position ASC')->fetchAll();

		if (empty($data) === false) {
			foreach ($data as $i) {
				$itemStructure = $this->getItemFromStructure($i->itemId);
				$itemStructureData = $this->getItemFromStructureData($i->itemId);
				$subs = $this->getMenuSubs($i->id);
				$n = [
					'id' => $i->itemId,
					'url' => '\\\\' . $this->baseUrl . $this->lang_url . $itemStructureData->url,
					'name' => $itemStructureData->menuText,
					'menuCssClass' => $itemStructureData->menuCssClass
				];

					$n += ['subs' => $subs];

				if ($itemStructure->status) {
					$menuData[] = (object)$n;
				}
			}

			return $menuData;
		}
	}

	public function getMenu($parent_id, $allow_subs)
	{
		$this->menuData = array();
		$data = $this->table(self::TABLE_MENU)->where(['parent_id' => $parent_id, 'status' => 1])->order('parent_id, position ASC')->fetchAll();

		foreach ($data as $i) {
			$itemStructure = $this->getItemFromStructure($i->itemId);
			$itemStructureData = $this->getItemFromStructureData($i->itemId);
			$subs = $this->getMenuSubs($i->id);
			$n = array(
				'id' => $i->itemId,
				'url' => '\\\\' . $this->baseUrl . $this->lang_url . $itemStructureData->url,
				'name' => $itemStructureData->menuText,
				'menuCssClass' => $itemStructureData->menuCssClass
			);

			if ($allow_subs) {
				$n += array('subs' => $subs);
			}

			if ($itemStructure->status) {
				$this->menuData[] = (object)$n;
			}
		}

		return $this->menuData;
	}

	public function printMenu($parent_id, $tpl = 'components/classicMenu.latte', $class = '', $allow_subs = true)
	{
		$data = $this->getMenu($parent_id, $allow_subs);
//		print_r($data);exit();
//		return $status;

		$template = new FileTemplate(__DIR__.'/../../presenters/templates/'.$tpl);
		$template->registerFilter(new Nette\Latte\Engine);
		$template->registerHelperLoader('Nette\Templating\Helpers::loader');
		$template->class = $class;
		$template->data = $data;
		$template->render();

		return;
	}

	public function getItemFromStructure($key)
	{
		$item = $this->table(self::TABLE_STRUCTURE)->where('id',$key)->fetch();

		return $item;
	}

	public function getItemFromStructureData($key)
	{
		$item = $this->table(self::TABLE_STRUCTURE_DATA)->select('
		*,
		menuText_'.$this->lang.' AS menuText,
		title_'.$this->lang.' AS title,
		h1_'.$this->lang.' AS h1,
		h2_'.$this->lang.' AS h2,
		description_'.$this->lang.' AS description,
		keywords_'.$this->lang.' AS keywords,
		anotace_'.$this->lang.' AS anotace,
		text1_'.$this->lang.' AS text1,
		text2_'.$this->lang.' AS text2
		')->where('itemId',$key)->fetch();

		return $item;
	}

	/**
	 * ---------------
	 * --------------- Data stránek: nadpis, url, atd....
	 * ---------------
	 */
	public function getPageById($key)
	{
		$data = [];
		$status = $this->getItemFromStructureData($key);

		foreach ($status as $key => $value) {
			if ($key == 'url') {
				$value = '\\\\' . $this->baseUrl . $this->lang_url . $value;
			}
			$data[$key] = $value;
		}

		return (object) $data;
	}

	/**
	 * ---------------
	 * --------------- Vrátí slider
	 * ---------------
	 */
	public function getSlider($key)
	{
		$data = $this->table(self::TABLE_SLIDER)->where(['parent_id' => $key, 'status' => 1])->order('parent_id, position ASC')->fetchAll();

		return $data;
	}

	public function scrape_insta_hash($tag)
	{
		$insta_source = file_get_contents('https://www.instagram.com/explore/tags/'.$tag.'/'); // instagrame tag url
		$shards = explode('window._sharedData = ', $insta_source);
		$insta_json = explode(';</script>', $shards[1]);
		$insta_array = json_decode($insta_json[0], TRUE);
		return $insta_array; // this return a lot things print it and see what else you need
	}

	public function instagram($tag, $limit = 9)
	{
		/*
		* https://github.com/AH72KING/Instagram-scraping/blob/master/instagram_hashtag_images.php
		*/
		if ($limit > 15) {
			$limit = 15;
		}
		$results_array = $this->scrape_insta_hash($tag);
		$output = [];

		for ($i=0; $i < $limit; $i++) {
			$output[] = (object) $results_array['entry_data']['TagPage'][0]['tag']['media']['nodes'][$i];
		}

		return $output;
	}


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}



}

