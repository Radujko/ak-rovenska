<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


/**
 * Users management.
 * user roles from higgest - admin, editor, moderator, member, user, quest
 */
class Contact extends Nette\Object
{

	const 
		TABLE = 'contact';

	/** @var Nette\Database\Context */
	private $database;

	/** @var App\Model\Presets */
	public $presets;


	public function __construct(Nette\Database\Context $database, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->presets = $presets->getPresets();
	}


	public function addContact($values)
	{
		
		$rows = $this->table(self::TABLE)->insert($values);
		
		return $rows;
	}


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}



}

