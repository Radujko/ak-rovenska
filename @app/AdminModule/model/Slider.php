<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


class Slider extends Nette\Object
{
    const
	    TABLE = 'slider';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;

	public $path;

	public $menuData;

	public $totalChildrens;

    /**
     * @param Nette\Database\Connection $database
     */
	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}

	public function getSliderEdit($parent_id = 0)
	{
		$this->menuData = array();
		$data = $this->table(self::TABLE)->where('parent_id',$parent_id)->order('parent_id, position ASC')->fetchAll();

		foreach ($data as $i) {
			$n = array(
				'id' => $i->id,
				'p1' => $i->p1,
				'p2' => $i->p2,
				'p3' => $i->p3,
				'url' => $i->url,
				'name' => $i->name,
				'image' => $i->image,
				'status' => $i->status,
				'parent_id' => $i->parent_id
			);

			$this->menuData[] = (object)$n;
		}

		return $this->menuData;
	}

	public function getSliderDefault()
	{
		$menuData = array();
		$data = $this->table(self::TABLE)->where('parent_id',0)->order('parent_id, position ASC')->fetchAll();

		foreach ($data as $i) {
			$n = array(
				'id' => $i->id,
				'name' => $i->name,
				'count' => $this->getTotalChildrens($i->id)
			);

			$menuData[] = (object)$n;
		}

		return $menuData;
	}

	public function getTotalChildrensSubs($parent_id = 0)
	{
		$this->totalChildrens++;

		$data = $this->table(self::TABLE)->select('id')->where('parent_id',$parent_id)->fetchAll();

		if (empty($data) === false) {
			foreach ($data as $i) {
				$this->getTotalChildrensSubs($i->id);
			}
		}
	}

	public function getTotalChildrens($parent_id = 0)
	{
		$this->totalChildrens = 0;
		$data = $this->table(self::TABLE)->select('id')->where('parent_id',$parent_id)->fetchAll();

		foreach ($data as $i) {
			$this->getTotalChildrensSubs($i->id);
		}

		return $this->totalChildrens;
	}

	public function getSliderByKey($key)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->fetch();

		return $status;
	}

	public function addSlider($data)
	{
		$status = $this->table(self::TABLE)->insert($data);

		return $status;
	}

	public function updateSlider($key, $data)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->update($data);

		return $status;
	}

	public function deleteImage($name)
	{
		$file = $this->path . 'slides/' . $name;

		if (file_exists($file)) {
			return unlink($file);
		}
	}

	public function deleteSlider($key)
	{
		$status = $this->table(self::TABLE)->where('id', $key)->delete();

		return $status;
	}

	public function deleteSlide($key)
	{
		$slide = $this->getSliderByKey($key);
		$this->deleteImage($slide->image);

		$status = $this->table(self::TABLE)->where('id', $key)->delete();

		return $status;
	}


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}

}

 
?>