<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


/**
 * Users management.
 * user roles from higgest - admin, editor, moderator, member, user, quest
 */
class Reference extends Nette\Object
{

	const
		TABLE = 'reference',
		IMAGES = 'reference_images';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;

	/** @var Nette\Http\Request */
	public $container;

	public $path;
	public $sizes;

	public $lang = 'cz';
	public $lang_url = '';

	public $baseUrl;


	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets, Nette\Http\Request $container)
	{
		$this->container = $container;
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}

	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}

	public function get_youtube_thumb($hash, $type = 'hqdefault')
	{
		if ($type == 'default' || $type == 'hqdefault' || $type == 'mqdefault' || $type == 'sddefault' || $type == 'maxresdefault'){

			$thumb_link = 'http://img.youtube.com/vi/'.$hash.'/'.$type.'.jpg';
		}
		else {
			return NULL;
		}

		return $thumb_link;
	}

	public function addReferences($values)
	{
		$values->inserted_by = $this->user_id;
		$values->inserted_date = date('Y-m-d H:i:s');

		$rows = $this->table(self::TABLE)->insert($values);

		return $rows;
	}


	public function getReferences($all = false, $paginator = false)
	{
		if ($all) {

			return $this->table(self::TABLE)->order('date DESC');
		} else if ($paginator) {
			return $this->table(self::TABLE)->where('visible', 1)->order('date DESC')->limit($paginator->getLength(), $paginator->getOffset());
		} else {
			return $this->table(self::TABLE)->where('visible', 1)->order('top DESC')->order('date DESC');
		}
	}

	public function countMaxReferences($all = false)
	{
		if ($all) {
			return $this->table(self::TABLE)->count();
		} else {
			return $this->table(self::TABLE)->where('visible', 1)->count();
		}
	}


	public function fullReferences($limit = false, $category = false, $top = true, $paginator = false, $events = null)
	{
		if ($events) {
			$references = self::getEvents(false, $paginator);
		}
		else {
			$references = self::getReferences(false, $paginator);
		}
		$return = array();
		$images = array();

		if (!$references) {
			return;
		}

		if ($top) {
			$reference = $references->where('top', 1);
		}

		if ($category) {
			if (!array_key_exists ($category, $this->presets->sections)) return $return;
			$reference = $references->where('section', $category);
		}

		if ($limit) $references = $references->limit($limit);

		if ($references->count() == 0) return array();

		//dump($this->presets->prefix.'reference_id');exit;

		foreach ($references as $reference) {
			$id = $reference->id;
			$image = false;
			$i = $this->table(self::IMAGES)->where($this->presets->prefix.'reference_id', $id)->order('cover DESC')->order('sort')->limit(1);
			if ($i->count() > 0) $image = $i->fetch()->name;

			$r = new \StdClass();
			$r->id = $reference->id;
			$r->date = $reference->date;

			$r->label = $reference->label;

			$r->headline_cz = $reference->headline_cz;
			$r->headline_de = $reference->headline_de;
			$r->headline_ru = $reference->headline_ru;
			$r->headline_en = $reference->headline_en;

			$r->desc = $reference->desc;
			$r->desc_cz = $reference->desc_cz;
			$r->desc_de = $reference->desc_de;
			$r->desc_ru = $reference->desc_ru;
			$r->desc_en = $reference->desc_en;

			$r->text_cz = $reference->text_cz;
			$r->text_de = $reference->text_de;
			$r->text_en = $reference->text_en;
			$r->text_ru = $reference->text_ru;

			$r->video = $reference->video;
			$r->video_thumb = $this->get_youtube_thumb($r->video);

			$r->section = $reference->section;
			$r->visible = $reference->visible;

			$r->images = $image;
			$return[] = $r;
		}

		return $return;

	}


	public function getFullReference($id)
	{
		$reference = self::getReference($id,true);

		if ($reference->count() > 0) {
			$reference = $reference->fetch();
		} else {
			return false;
		}

		$id = $reference->id;
		$images = $this->table(self::IMAGES)->where(['skeleton_reference_id' => $id, 'NOT cover' => 1])->order('sort')->fetchAll();

		$r = new \StdClass();
		$r->id = $reference->id;
		$r->date = $reference->date;

		$r->label = $reference->label;

		$r->headline_cz = $reference->headline_cz;
		$r->headline_de = $reference->headline_de;
		$r->headline_ru = $reference->headline_ru;
		$r->headline_en = $reference->headline_en;

		$r->desc = $reference->desc;
		$r->desc_cz = $reference->desc_cz;
		$r->desc_de = $reference->desc_de;
		$r->desc_ru = $reference->desc_ru;
		$r->desc_en = $reference->desc_en;

		$r->text_cz = $reference->text_cz;
		$r->text_de = $reference->text_de;
		$r->text_en = $reference->text_en;
		$r->text_ru = $reference->text_ru;

		$r->video = $reference->video;
		$r->video_thumb = $this->get_youtube_thumb($r->video);

		$r->section = $reference->section;
		$r->visible = $reference->visible;

		$r->images = $images;
		return $r;
	}

	public function getNextId()
	{
		$status = $this->database->query('SHOW TABLE STATUS LIKE \''.$this->presets->prefix.self::TABLE.'\'')->fetch();
		return $status->Auto_increment;
	}


	public function deactivateReference($id, $state)
	{
		return $this->table(self::TABLE)->where('id', $id)->update(array('visible' => $state));
	}


	public function deleteReference($id)
	{
		$this->table(self::IMAGES)->where('skeleton_reference_id', $id)->delete();
		return $this->table(self::TABLE)->where('id', $id)->delete();
	}


	public function getReference($id, $check = false)
	{
		if ($check) {
			return $this->table(self::TABLE)->where('id', $id);
		} else {
			return $this->table(self::TABLE)->where('id', $id)->fetch();
		}

	}


	public function editReference($values)
	{
		$id = $values->id;
		unset($values->id);
		$values->modified_by = $this->user_id;
		$values->modified_date = date('Y-m-d H:i:s');

		if (!isset($values->image)) $values->image = '0/0.jpg';

		return $this->table(self::TABLE)->where('id', $id)->update($values);

	}

	public function addImage($id, $name)
	{
		$this->table(self::IMAGES)->insert(array(
			'skeleton_reference_id' => $id,
			'name' => $name,
			'add_by' => $this->user_id,
			'add_time' => date('Y-m-d H:i:s'),
		));
	}


	public function primaryImage($image_id)
	{
		$image = $this->table(self::IMAGES)->where('id', $image_id)->fetch();
		$images = self::getImages($image->skeleton_reference_id);
		foreach ($images as $i) {
			$i->update(array('cover' => 0));
		}
		$image->update(array('cover' => 1));
		return true;
	}

	public function getCoverImage($id)
	{
		return $this->table(self::IMAGES)->where('skeleton_reference_id', $id)->where('cover', 1)->fetch();
	}

	public function getImages($id)
	{
		return $this->table(self::IMAGES)->where('skeleton_reference_id', $id);
	}


	public function deleteImage($id)
	{
		$files = [];
		$image = $this->table(self::IMAGES)->get($id);
		if (!$image) {
			return FALSE;
		}

		$mainImg = $this->path . 'r' . $image->skeleton_reference_id . '/' . $image->name;
		$dir = $this->path . 'r' . $image->skeleton_reference_id . '/';

		if (file_exists($mainImg)) {
			$files[] = $mainImg;
		}

		foreach ($this->sizes as $key => $value) {
			$i = $this->path . 'r' . $image->skeleton_reference_id . '/' . $key . '_' . $image->name;
			if (file_exists($i)) {
				$files[] = $i;
			}
		}

		foreach ($files as $f) {
			unlink($f);
		}

		$image->delete();

		$this->checkDirEmptyToDelete($dir);

		return TRUE;
	}

	public function checkDirEmptyToDelete($dir)
	{
		if ($this->is_dir_empty($dir)) {
			return rmdir($dir);
		}
	}

	public function is_dir_empty($dir)
	{
		if (!is_readable($dir)) return NULL;
		$handle = opendir($dir);
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				return FALSE;
			}
		}
		return TRUE;
	}


	public function deleteFile($id)
	{
		return $this->table(self::TABLE)->where('id', $id)->update(array('file' => '0/0'));
	}


	/* special function to get table names with prefix */


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}


}

