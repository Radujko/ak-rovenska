<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


/**
 * Users management.
 * user roles from higgest - admin, editor, moderator, member, user, quest
 */
class PageData extends Nette\Object
{

	const
		TABLE = 'structure',
		TABLE_PRILOHY = 'structure_prilohy',
		TABLE_IMAGES = 'structure_images',
		TABLE_DATA = 'structure_data',
		TABLE_MENU = 'menu',
		TABLE_NEWS = 'news',
		TABLE_GALLERY = 'gallery';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;

	public $lang = 'cz';

	public $breadcrumb = [];


	/**
	 * @param Nette\Database\Connection $database
	 */
	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets)
	{
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}


	public function getHomePage()
	{
		$status = $this->table(self::TABLE)->where(['homepage' => 1, 'status' => 1])->fetch();

		return $status;
	}

	public function getPageByUrl($url)
	{
		$id = $this->table(self::TABLE_DATA)->where(['url' => $url])->fetch();
		if ( isset($id->itemId) ) {
			$status = $this->table(self::TABLE)->where(['id' => $id->itemId, 'status' => 1])->fetch();
			return $status;
		}

		return false;
	}

	public function getPageByKey($key)
	{
		$status = $this->table(self::TABLE)->where(['id' => $key, 'status' => 1])->fetch();

		return $status;
	}

	public function getPageDataByKey($key)
	{
		$status = $this->table(self::TABLE_DATA)->select('
		*,
		menuText_'.$this->lang.' AS menuText,
		title_'.$this->lang.' AS title,
		h1_'.$this->lang.' AS h1,
		h2_'.$this->lang.' AS h2,
		description_'.$this->lang.' AS description,
		keywords_'.$this->lang.' AS keywords,
		anotace_'.$this->lang.' AS anotace,
		text1_'.$this->lang.' AS text1,
		text2_'.$this->lang.' AS text2
		')->where(['itemId' => $key])->fetch();

		return $status;
	}

	public function getPrilohyByKey($key)
	{
		return $this->table(self::TABLE_PRILOHY)->where('track_id', $key)->fetchAll();
	}

	public function getGalleryByKey($key)
	{
		return $this->table(self::TABLE_IMAGES)->where('track_id', $key)->fetchAll();
	}

	public function getHighestParentInMenu($key)
	{
		$parent_id = $key;
		$item = $this->table(self::TABLE_MENU)->where(['id' => $key])->fetch();

		if ($item->parent_id != 0) {
			return $this->getHighestParentInMenu($item->parent_id);
		}

		return $parent_id;
	}

	public function getBreadcrumbChilds($key)
	{
		$parent_id = $key;

		$menuPage = $this->table(self::TABLE_MENU)->where(['id' => $parent_id])->fetch();
		$data = $this->table(self::TABLE_DATA)->select('url, menuText_'.$this->lang.' AS menuText')->where(['itemId' => $menuPage->itemId])->fetch();

		$this->breadcrumb[] = $data;

		if ($menuPage->parent_id != 0) {
			return $this->getBreadcrumbChilds($menuPage->parent_id);
		}

		return true;
	}

	public function addNewIntoBreadcrumb($id)
	{
		$new = $this->table(self::TABLE_NEWS)->select('headline_'.$this->lang.' AS headline')->where('id', $id)->fetch();
		if (!$new) {
			return;
		}

		$data = (object) [
			'url' =>  end($this->breadcrumb)->url . '/' . $id . '/' . Nette\Utils\Strings::webalize($new->headline),
			'menuText' => $new->headline
		];

		$this->breadcrumb[] = $data;

		return true;
	}

	public function addGalleryIntoBreadcrumb($id)
	{
		$gpage = $this->table(self::TABLE_GALLERY)->select('headline_'.$this->lang.' AS headline')->where('id', $id)->fetch();
		if (!$gpage) {
			return;
		}

		$data = (object) [
			'url' =>  end($this->breadcrumb)->url . '/' . $id . '/' . Nette\Utils\Strings::webalize($gpage->headline),
			'menuText' => $gpage->headline
		];

		$this->breadcrumb[] = $data;

		return true;
	}

	public function getBreadcrumb($key, $menuId, $type = NULL, $type_id = 0)
	{
		$maybe = $this->table(self::TABLE_MENU)->where(['itemId' => $key])->fetchAll();
		if ($maybe) {
			foreach ($maybe as $m) {
				if ($this->getHighestParentInMenu($m->id) == $menuId) {
					$this->getBreadcrumbChilds($m->id);
				}
			}
		}

		$this->breadcrumb = array_reverse($this->breadcrumb);

		if ($type == 'new') {
			$this->addNewIntoBreadcrumb($type_id);
		}

		if ($type == 'gallery') {
			$this->addGalleryIntoBreadcrumb($type_id);
		}

		return $this->breadcrumb;
	}


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}



}

