<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;
use Nette\Utils\Strings;


/**
 * Users management.
 * user roles from higgest - admin, editor, moderator, member, user, quest
 */
class News extends Nette\Object
{

	const 
		TABLE = 'news',
		IMAGES = 'news_images';

	/** @var Nette\Database\Context */
	private $database;

	/** @var Nette\Security\User */
	private $user_id;

	/** @var App\Model\Presets */
	public $presets;

	/** @var Nette\Http\Request */
	public $container;

	public $path;
	public $sizes;

	public $lang = 'cz';
	public $lang_url = '';

	public $baseUrl;

	public $newsByFilter = false;
	public $filterParams = ['category_kv_arena', 'category_pool_center', 'category_sports_hall', 'category_training_hall'];
	public $filters = [];


	public function __construct(Nette\Database\Context $database, Nette\Security\User $user, \App\Model\Presets $presets, Nette\Http\Request $container)
	{
		$this->container = $container;
		$this->database = $database;
		$this->user_id = @$user->getIdentity()->id;
		$this->presets = $presets->getPresets();
	}

	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}

	public function get_youtube_thumb($hash, $type = 'hqdefault')
	{
		if ($type == 'default' || $type == 'hqdefault' || $type == 'mqdefault' || $type == 'sddefault' || $type == 'maxresdefault'){

			$thumb_link = 'http://img.youtube.com/vi/'.$hash.'/'.$type.'.jpg';
		}
		else {
			return NULL;
		}

		return $thumb_link;
	}

	public function getFilters()
	{
		if ($this->newsByFilter) {

			foreach ($this->filterParams as $p) {
				$state = $this->container->getCookie($p);
				if ($state == null || $state == 'true') {
					$this->filters[$p] = TRUE;
				}
			}

		}
		return $this->filters;
	}

	public function addNews($values)
	{
		$values->inserted_by = $this->user_id;
		$values->inserted_date = date('Y-m-d H:i:s');
		
		$rows = $this->table(self::TABLE)->insert($values);
		
		return $rows;
	}


	public function getNews($all = false, $paginator = false)
	{
		$filters = $this->getFilters();

		if (count($filters) == 0 && $this->newsByFilter) {
			return FALSE;
		}

		if ($all) {

			return $this->table(self::TABLE)->whereOr($filters)->order('date DESC');
		} else if ($paginator) {
			return $this->table(self::TABLE)->whereOr($filters)->where('visible', 1)->order('date DESC')->limit($paginator->getLength(), $paginator->getOffset());
		} else {
			return $this->table(self::TABLE)->whereOr($filters)->where('visible', 1)->order('top DESC')->order('date DESC');
		}
	}


	public function getEvents($all = false, $paginator = false)
	{

		if ($all) {
			return $this->table(self::TABLE)->where(['section' => 'event'])->order('date DESC');
		} else if ($paginator) {
			return $this->table(self::TABLE)->where(['section' => 'event', 'visible' => 1])->order('date DESC')->limit($paginator->getLength(), $paginator->getOffset());
		} else {
			return $this->table(self::TABLE)->select('*, headline_'.$this->lang.' AS headline, desc_'.$this->lang.' AS desc')->where(['section' => 'event', 'visible' => 1])->order('top DESC')->order('date DESC');
		}
	}

	public function countMaxNews($all = false)
	{
		$filters = $this->getFilters();

		if (count($filters) == 0 && $this->newsByFilter) {
			return FALSE;
		}

		if ($all) {
			return $this->table(self::TABLE)->whereOr($filters)->count();
		} else {
			return $this->table(self::TABLE)->whereOr($filters)->where('visible', 1)->count();
		}
	}

	
	public function fullNews($limit = false, $category = false, $top = true, $paginator = false, $events = null)
	{
		if ($events) {
			$news = self::getEvents(false, $paginator);
		}
		else {
			$news = self::getNews(false, $paginator);
		}
		$return = array();
		$images = array();

		if (!$news) {
			return;
		}

		if ($top) {
			$new = $news->where('top', 1);	
		}

		if ($category) {
			if (!array_key_exists ($category, $this->presets->sections)) return $return;
			$new = $news->where('section', $category);	
		} 

		if ($limit) $news = $news->limit($limit);

		if ($news->count() == 0) return array();

		//dump($this->presets->prefix.'news_id');exit;

		foreach ($news as $new) {
			$id = $new->id;
			$image = false;
			$i = $this->table(self::IMAGES)->where($this->presets->prefix.'news_id', $id)->order('cover DESC')->order('sort')->limit(1);
			if ($i->count() > 0) $image = $i->fetch()->name;
			
			$r = new \StdClass();
			$r->id = $new->id;
			$r->date = $new->date;

			$r->label = $new->label;
			
			$r->headline_cz = $new->headline_cz;
			$r->headline_de = $new->headline_de;
			$r->headline_ru = $new->headline_ru;
			$r->headline_en = $new->headline_en;
			
			$r->desc = $new->desc;
			$r->desc_cz = $new->desc_cz;
			$r->desc_de = $new->desc_de;
			$r->desc_ru = $new->desc_ru;
			$r->desc_en = $new->desc_en;

			$r->text_cz = $new->text_cz;
			$r->text_de = $new->text_de;
			$r->text_en = $new->text_en;
			$r->text_ru = $new->text_ru;

			$r->video = $new->video;
			$r->video_thumb = $this->get_youtube_thumb($r->video);

//			if ($new->category_kv_arena) { $r->category_kv_arena = 'KV Arena'; }
//			if ($new->category_pool_center) { $r->category_pool_center = 'Bazénové centrum'; }
//			if ($new->category_sports_hall) { $r->category_sports_hall = 'Hala míčových sportů'; }
//			if ($new->category_training_hall) { $r->category_training_hall = 'Tréninková hala'; }
			/*
			$r->category_kv_arena = $new->category_kv_arena;
			$r->category_pool_center = $new->category_pool_center;
			$r->category_sports_hall = $new->category_sports_hall;
			$r->category_training_hall = $new->category_training_hall;
			*/

			$r->section = $new->section;
			$r->visible = $new->visible;
			
			$r->images = $image;
			$return[] = $r;
		}

		return $return;

	}


	public function getFullNew($id)
	{
		$new = self::getNew($id,true);
	
		if ($new->count() > 0) {
			$new = $new->fetch();
		} else {
			return false;
		}

		$id = $new->id;
		$images = $this->table(self::IMAGES)->where(['skeleton_news_id' => $id, 'NOT cover' => 1])->order('sort')->fetchAll();
			
		$r = new \StdClass();
		$r->id = $new->id;
		$r->date = $new->date;

		$r->label = $new->label;
		
		$r->headline_cz = $new->headline_cz;
		$r->headline_de = $new->headline_de;
		$r->headline_ru = $new->headline_ru;
		$r->headline_en = $new->headline_en;
		
		$r->desc = $new->desc;
		$r->desc_cz = $new->desc_cz;
		$r->desc_de = $new->desc_de;
		$r->desc_ru = $new->desc_ru;
		$r->desc_en = $new->desc_en;

		$r->text_cz = $new->text_cz;
		$r->text_de = $new->text_de;
		$r->text_en = $new->text_en;
		$r->text_ru = $new->text_ru;
	
		$r->video = $new->video;
		$r->video_thumb = $this->get_youtube_thumb($r->video);

		$r->section = $new->section;
		$r->visible = $new->visible;
		
		$r->images = $images;
		return $r;
	}


	public function getNextId()
	{
		$rows = $this->table(self::TABLE)->order('id DESC')->limit(1);
		if ($rows->count() > 0) {
			return $rows->fetch()->id + 1;
		} else {
			return 1;
		}

	}


	public function deactivateNews($id, $state)
	{
		return $this->table(self::TABLE)->where('id', $id)->update(array('visible' => $state));
	}


	public function deleteNew($id)
	{
		$this->table(self::IMAGES)->where('skeleton_news_id', $id)->delete();
		return $this->table(self::TABLE)->where('id', $id)->delete();
	}


	public function getNew($id, $check = false)
	{
		if ($check) {
			return $this->table(self::TABLE)->where('id', $id);
		} else {
			return $this->table(self::TABLE)->where('id', $id)->fetch();	
		}
		
	}
	

	public function editNews($values)
	{
		$id = $values->id;
		unset($values->id);
		$values->modified_by = $this->user_id;
		$values->modified_date = date('Y-m-d H:i:s');

		if (!isset($values->image)) $values->image = '0/0.jpg';

		return $this->table(self::TABLE)->where('id', $id)->update($values);

	}

	public function addImage($id, $name)
	{
		$this->table(self::IMAGES)->insert(array(
			'skeleton_news_id' => $id,
			'name' => $name,
			'add_by' => $this->user_id,
			'add_time' => date('Y-m-d H:i:s'),
		));
	}


	public function primaryImage($image_id)
	{
		$image = $this->table(self::IMAGES)->where('id', $image_id)->fetch();
		$images = self::getImages($image->skeleton_news_id);
		foreach ($images as $i) {
			$i->update(array('cover' => 0));
		}
		$image->update(array('cover' => 1));
		return true;
	}

	public function getCoverImage($id)
	{
		return $this->table(self::IMAGES)->where('skeleton_news_id', $id)->where('cover', 1)->fetch();
	}

	public function getImages($id) 
	{
		return $this->table(self::IMAGES)->where('skeleton_news_id', $id);
	}


	public function deleteImage($id)
	{
		$files = [];
		$image = $this->table(self::IMAGES)->get($id);
		if (!$image) {
			return FALSE;
		}

		$mainImg = $this->path . $image->skeleton_news_id . '/' . $image->name;
		$dir = $this->path . $image->skeleton_news_id . '/';

		if (file_exists($mainImg)) {
			$files[] = $mainImg;
		}

		foreach ($this->sizes as $key => $value) {
			$i = $this->path . $image->skeleton_news_id . '/' . $key . '_' . $image->name;
			if (file_exists($i)) {
				$files[] = $i;
			}
		}

		foreach ($files as $f) {
			unlink($f);
		}

		$image->delete();

		$this->checkDirEmptyToDelete($dir);

		return TRUE;
	}

	public function checkDirEmptyToDelete($dir)
	{
		if ($this->is_dir_empty($dir)) {
			return rmdir($dir);
		}
	}

	public function is_dir_empty($dir)
	{
		if (!is_readable($dir)) return NULL;
		$handle = opendir($dir);
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				return FALSE;
			}
		}
		return TRUE;
	}


	public function deleteFile($id)
	{
		return $this->table(self::TABLE)->where('id', $id)->update(array('file' => '0/0'));
	}


	/* special function to get table names with prefix */


	private function table($name)
	{
		return $this->database->table($this->presets->prefix . $name);
	}


}

