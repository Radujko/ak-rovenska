<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\Image;
use Tracy\Debugger;


class ContactForm extends Nette\Object
{
	
	/** @var Contact */
	private $contact;

	/** @var Presets */
	private $presets;

	/** @var Emails */
	private $emails;


	public function __construct(\App\Model\Contact $contact, \App\Model\Emails $emails, \App\Model\Presets $presets)
	{
		$this->presets = $presets->getPresets();
		$this->contact = $contact;
		$this->emails = $emails;
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		
		$form = new Form;

		$form->addText('name', 'Jméno a příjmení')
			->setAttribute('class', 'text-input')
			->setRequired('Zadejte prosím jméno a příjmení.');

		$form->addText('email', 'Email')
			->setAttribute('class', 'text-input')
			->setRequired('Zadejte prosím váš email.');

		$form->addText('phone', 'Telefon')
			->setAttribute('class', 'text-input')
			->setRequired('Zadejte prosím váš telefon.');
		
		$form->addTextarea('message', 'Zpráva')
			->setAttribute('class', 'textarea-input')
			->addRule(Form::MIN_LENGTH, 'Zadejte váš dotaz.', 3)
			->setRequired('Zadejte vaši zprávu.');
		
		$form->addSubmit('send', 'Odeslat')
			 ->setAttribute('class', 'button button-send');
		
		$form->onSuccess[] = array($this, 'formSucceeded');
		
		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {

			$values->created = date("Y-m-d H:i:s",strtotime('now'));
			
			$save = $this->contact->addContact($values);

			if ($save) {
				$this->emails->sendContact($values);
			} else {
				$form->addError('An error occured, please try again.');
			}
		} catch (\Exception $e) {
			Debugger::log('Problem with contact form: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError('An error occured, please try again.');
		}
	}

}
