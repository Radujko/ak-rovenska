<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\Strings;
use Nette\Utils\Image;
use Tracy\Debugger;


class EditNewsForm extends Nette\Object
{
	
	/** @var News */
	private $news;

	/** @var Presets */
	private $presets;

	/** @var Sizes */
	private $sizes;

	public $path;
	public $id;
	public $lang;



	public function __construct(\App\Model\News $news, \App\Model\Presets $presets)
	{
		$this->presets = $presets->getPresets();
		$this->news = $news;
	}



	public function setId($id)
	{
		$this->id = $id;
	}

	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}


	/**
	 * @return Form
	 */
	public function create($lang)
	{

		$news = $this->news->getNew($this->id);
		$lang = '_' . Strings::truncate($lang,2,'');
		$this->lang = $lang;

		$form = new Form;

		$form->addHidden('id', $this->id);



		$form->addText('date', 'Datum:*')
			->setAttribute('class', 'date')
			->setRequired('Vyplňte datum.')
			->setAttribute('id','dateNew');


		if ($this->presets->news['sections']) {
			$form->addSelect('section', 'Typ příspěvku:*', $this->presets->sections)
				->setPrompt('-- vyberte typ --')
				->setRequired('Vyberte typ příspěvku');
		}

		if ($this->presets->news['labels']) {
			$form->addSelect('label', 'Barva:*', $this->presets->newsLabel)
				->setPrompt('-- barva --');
		}

//		$form->addCheckbox('category_kv_arena', 'KV Arena');
//		$form->addCheckbox('category_pool_center', 'Bazénové centrum');
//		$form->addCheckbox('category_sports_hall', 'Hala míčových sportů');
//		$form->addCheckbox('category_training_hall', 'Tréninková hala');

		$form->addText('headline'.$lang, 'Nadpis:*')
			->addRule(Form::MAX_LENGTH, 'Nadpis může mít maximálně %d znaků', 100)
			->setRequired('Vyplňte nadpis.');

		$form->addTextArea('desc'.$lang, 'Krátký popis:*')
			->addRule(Form::MAX_LENGTH, 'Popis může mít maximálně %d znaků', 1000)
			->setRequired('Vyplňte popis.');

		if ($this->presets->news['richDesc']) {
			$form->addTextarea('text'.$lang, 'Text novinky:')
				->setAttribute('class', 'tinymce');
		}

		$form->addText('video', 'Video:')
			->setAttribute('title', 'Např.: https://www.youtube.com/watch?v=NFZLrfiBTm8')
			->setAttribute('placeholder', 'Např.: https://www.youtube.com/watch?v=NFZLrfiBTm8');

		$form->addCheckbox('top', 'Top - na úvodní stránce? (limit 4)')
			 ->setAttribute('class', 'top-checkbox');

		// setDefaults
		foreach ($form->components as $key => $value) {
			if ($news->$key) {
				if ($key != 'id') {
					$form[$key]->setDefaultValue($news->$key);
				}
			}
		}

		$form['date']->setDefaultValue(date("j.n.Y G:i",strtotime($news->date)));

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		$values->date = date("Y-m-d H:i:s",strtotime($values->date));


		if ($values->video) {
			$values->video = self::getVideoId($values->video);
		}


		try {
			$this->news->editNews($values);
		} catch (\Exception $e) {
			Debugger::log('Problem with edit new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}


	private function getVideoId($video)
	{
		$video = (string) $video;

		if (strpos($video, 'http://youtu.be/') !== false ) {
			return str_replace('http://youtu.be/', '', $video);
		} 

		if (strpos($video, 'http://www.youtube.com/watch?v=') !== false ) {
			return str_replace('http://www.youtube.com/watch?v=', '', $video);
		}

		if (strpos($video, 'https://www.youtube.com/watch?v=') !== false ) {
			return str_replace('https://www.youtube.com/watch?v=', '', $video);
		} 

		if (strpos($video, 'http://youtube.com/watch?v=') !== false ) {
			return str_replace('http://youtube.com/watch?v=', '', $video);
		} 

		if (strpos($video, 'https://youtube.com/watch?v=') !== false ) {
			return str_replace('https://youtube.com/watch?v=', '', $video);
		} 

		return '';

	}

}
