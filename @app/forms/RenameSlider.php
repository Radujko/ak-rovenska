<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tracy\Debugger;
use Nette\Utils\Html;


class RenameSliderForm extends Nette\Object
{

	/** @var \App\Model\Slider @inject */
	public $slider;


	public function __construct(\App\Model\Slider $slider)
	{
		$this->slider = $slider;
	}


	/**
	 * @return Form
	 */
	public function create()
	{

		$form = new Form;


		$form->addHidden('id', '0');

		$form->addText('name', 'Název:*')
			->setRequired('Zadejte název stránky.');

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {
			$key = $values->id;
			unset($values->id);

			$this->slider->updateSlider($key, $values);

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
