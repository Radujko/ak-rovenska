<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tracy\Debugger;
use Nette\Utils\Html;


class EditPageStructureAdvancedForm extends Nette\Object
{

	/** @var \App\Model\Structure @inject */
	public $structure;

	public $id;


	public function __construct(\App\Model\Structure $structure)
	{
		$this->structure = $structure;
	}

	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		$page = $this->structure->getPageByKey($this->id);
		$pageData = $this->structure->getPageDataByKey($this->id);


		$form = new Form;


		$form->addHidden('id', $this->id);

		$form->addText('bodyCssClass', 'Body css class:')
			->setValue($pageData->bodyCssClass);

		$form->addText('menuCssClass', 'Menu css class:')
			->setValue($pageData->menuCssClass);

		$form->addText('urlToRedirect', 'Url pro přesměrování:')
			->setValue($pageData->urlToRedirect)
			->setAttribute('placeholder', 'http://...');

		$form->addText('redirectFromUrl', 'Přesměrování z url:')
			->setValue($pageData->redirectFromUrl);

		$form->addCheckbox('homepage', 'Homepage:')
			->setDefaultValue($page->homepage);

		if ($page->homepage) {
			$form['homepage']->setDisabled();
		}

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {
			$key = $values->id;
			unset($values->id);

			$homepage = $values->homepage;
			unset($values->homepage);

			$this->structure->updatePageData($key, $values);
			$this->structure->updateAllPages(['homepage' => 0]);
			$this->structure->updatePage($key, ['homepage' => $homepage]);

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
