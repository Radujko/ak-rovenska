<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\Image;
use Tracy\Debugger;
use Nette\Utils\Html;


class AddStructurePrilohyForm extends Nette\Object
{

	/** @var \App\Model\Structure @inject */
	public $structure;

	/** @var Presets */
	private $presets;

	public $path;
	public $sizes;


	public function __construct(\App\Model\Structure $structure, \App\Model\Presets $presets)
	{
		$this->presets = $presets->getPresets();
		$this->structure = $structure;
	}


	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}

	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		
		$form = new Form;

		$max = 999 - $this->structure->getPrilohy($this->id)->count();

		$form->addMultiUpload('files', 'Soubory')
			->setAttribute('class', 'image-input')
			->addRule(Form::MAX_LENGTH, 'Nahrajte maximálně %d soubory', $max)
			->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 8 mB.', 8 * 1024 * 1024 /* v bytech */)
			->setOption('description', Html::el('p')
				->setHtml('<em style="font-style: italic; display: inline-block; padding: 10px 0;line-height: 20px;">Více souborů můžete vybrat při současném držení CTRL. Nyní můžete vybrat maximálně '.$max.' soubor/y.</em>')
			);
			

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		$id = $this->id;

		if (count($values->files) > 0) {
			$files = $values->files;
			foreach ($files as $file) {
				if ($file->isOk()) {
					$path = $this->path . 's' . $id . '/' . $file->getSanitizedName();
					$name = $file->getSanitizedName();
					$p_name = pathinfo($file->getName(),PATHINFO_FILENAME);
					$ext = pathinfo($file->getName(),PATHINFO_EXTENSION);
					$size = $file->getSize();
					$file->move($path);

					$this->structure->addPrilohy($id, $name, $p_name, $ext, $size);
				} else {
					unset($values->file);
				}
			}
		} 

	}




}
