<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;
use Nette\Utils\Html;


class SearchForm extends Nette\Object
{

	/** @var Search @inject */
	public $search;

	/** @var Presets @inject */
	public $presets;

	public $actionUrl;

	public $needle;



	public function __construct(\App\Model\Search $search, \App\Model\Presets $presets)
	{
		$this->presets = $presets->getPresets();
		$this->search = $search;
	}

	/**
	 * @return Form
	 */
	public function create()
	{

		$form = new Form;

		$form->setAction($this->actionUrl);
		$form->setMethod('GET');

		$form->addText('needle')
			->setDefaultValue($this->needle)
			->setAttribute('placeholder', 'Vyhledávání')
			->setRequired('Zadejte hledaný dotaz.');

		$form->addSubmit('search', '')
			->setAttribute('class', 'ico-search inline-block');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {

		} catch (\Exception $e) {
			Debugger::log('Problem with searching: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
