<?php

namespace App\Forms;

use App\Model\Gallery;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\Strings;
use Nette\Utils\Image;
use Tracy\Debugger;
use Nette\Utils\Html;


class AddGalleryForm extends Nette\Object
{
	
	/** @var Gallery */
	private $gallery;

	/** @var Presets */
	private $presets;

	public $path;
	public $sizes;
	public $lang;


	public function __construct(\App\Model\Gallery $gallery, \App\Model\Presets $presets)
	{
		$this->presets = $presets->getPresets();
		$this->gallery = $gallery;
	}


	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}


	/**
	 * @return Form
	 */
	public function create($lang)
	{
		
		$lang = '_' . Strings::truncate($lang,2,'');
		$this->lang = $lang;


		$form = new Form;



		$form->addText('date', 'Datum:*')
			->setAttribute('class', 'date hasDatepicker')
			->setRequired('Vyplňte datum.')
			->setAttribute('id','dateNew');

		$form->addCheckbox('category_foto', 'Foto')
			->setAttribute('class', 'checkbox');
		$form->addCheckbox('category_video', 'Video')
			->setAttribute('class', 'checkbox');
		$form->addCheckbox('category_kultura', 'Kultura')
			->setAttribute('class', 'checkbox');
		$form->addCheckbox('category_sport', 'Sport')
			->setAttribute('class', 'checkbox');

		$form->addText('headline'.$lang, 'Nadpis:*')
			->addRule(Form::MAX_LENGTH, 'Nadpis může mít maximálně %d znaků', 100)
			->setRequired('Vyplňte nadpis.');


		if ($this->presets->news['richDesc']) {
			$form->addTextarea('text'.$lang, 'Text galerie:')
				->setAttribute('class', 'tinymce');
		}

		$form->addText('video', 'Video:')
			->setAttribute('title', 'Např.: https://www.youtube.com/watch?v=NFZLrfiBTm8')
			->setAttribute('placeholder', 'Např.: https://www.youtube.com/watch?v=NFZLrfiBTm8');

		$form->addMultiUpload('files', 'Soubory')
			->addRule(Form::IMAGE, 'Soubory musí být JPEG, PNG nebo GIF.')
			->addRule(Form::MAX_LENGTH, 'Nahrajte maximálně 999 souborů', 999)
			->setOption('description', Html::el('p')
				->setHtml('<em style="font-style: italic; display: inline-block; padding: 10px 0;line-height: 20px;">Více souborů můžete vybrat při současném držení CTRL. Můžete vybrat maximálně 999 souborů.</em>')
			);

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{
		$values->date = date("Y-m-d",strtotime($values->date));

		if ($values->video) {
			$values->video = self::getVideoId($values->video);
		}

		$files = $values->files;
		unset($values->files);


		try {

			$id = $this->gallery->addPage($values);

			if (count($files) > 0) {
				foreach ($files as $file) {
					if ($file->isOk()) {
						$path = $this->path . 'g' . $id . '/' . $file->getSanitizedName();
						$name = $file->getSanitizedName();
						$file->move($path);

						foreach ($this->sizes as $key => $size) {
							$sizes = explode('/', $size);
							$quality = $sizes[2] * 100;
							$image = Image::fromFile($path);

							if ($sizes[3] == 'EXACT') {
								$image->resize($sizes[0], $sizes[1], Image::EXACT);
							} elseif ($sizes[3] == 'FILL') {
								$image->resize($sizes[0], $sizes[1], Image::SHRINK_ONLY | Image::FILL);
							} elseif ($sizes[3] == 'SHRINK_ONLY') {
								$image->resize($sizes[0], $sizes[1], Image::SHRINK_ONLY);
							} elseif ($sizes[3] == 'STRETCH') {
								$image->resize($sizes[0], $sizes[1], Image::STRETCH);
							} else {
								$image->resize($sizes[0], $sizes[1]);
							}

							$p = $this->path . 'g' . $id . '/' . $key . '_' . $name;
							$image->save($p, $quality, Image::JPEG);
						}

						$this->gallery->addImage($id, $name);
					}
				}
			}

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}


	private function getVideoId($video)
	{
		$video = (string) $video;

		if (strpos($video, 'http://youtu.be/') !== false ) {
			return str_replace('http://youtu.be/', '', $video);
		} 

		if (strpos($video, 'http://www.youtube.com/watch?v=') !== false ) {
			return str_replace('http://www.youtube.com/watch?v=', '', $video);
		}

		if (strpos($video, 'https://www.youtube.com/watch?v=') !== false ) {
			return str_replace('https://www.youtube.com/watch?v=', '', $video);
		} 

		if (strpos($video, 'http://youtube.com/watch?v=') !== false ) {
			return str_replace('http://youtube.com/watch?v=', '', $video);
		} 

		if (strpos($video, 'https://youtube.com/watch?v=') !== false ) {
			return str_replace('https://youtube.com/watch?v=', '', $video);
		} 

		return '';

	}

}
