<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\Strings;
use Nette\Utils\Image;
use Tracy\Debugger;
use Nette\Utils\Html;


class AddNewsForm extends Nette\Object
{
	
	/** @var News */
	private $news;

	/** @var Presets */
	private $presets;

	public $path;
	public $sizes;
	public $lang;


	public function __construct(\App\Model\News $news, \App\Model\Presets $presets)
	{
		$this->presets = $presets->getPresets();
		$this->news = $news;
	}


	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}


	/**
	 * @return Form
	 */
	public function create($lang)
	{
		
		$lang = '_' . Strings::truncate($lang,2,'');
		$this->lang = $lang;


		$form = new Form;

		if (!$this->presets->news['use']) return $form;

		$form->addText('date', 'Datum:*')
			->setAttribute('class', 'date hasDatepicker')
			->setRequired('Vyplňte datum.')
			->setAttribute('id','dateNew');

		if ($this->presets->news['sections']) {
			$form->addSelect('section', 'Typ příspěvku:*', $this->presets->sections)
				->setPrompt('-- vyberte sekci --')
				->setRequired('Vyberte sekci aktuality');
		}

		if ($this->presets->news['labels']) {
			$form->addSelect('label', 'Barva:*', $this->presets->newsLabel)
				->setPrompt('-- barva --');
		}

//		$form->addCheckbox('category_kv_arena', 'KV Arena')
//			->setAttribute('class', 'checkbox');
//		$form->addCheckbox('category_pool_center', 'Bazénové centrum')
//			->setAttribute('class', 'checkbox');
//		$form->addCheckbox('category_sports_hall', 'Hala míčových sportů')
//			->setAttribute('class', 'checkbox');
//		$form->addCheckbox('category_training_hall', 'Tréninková hala')
//			->setAttribute('class', 'checkbox');
			
		

		$form->addText('headline'.$lang, 'Nadpis:*')
			->addRule(Form::MAX_LENGTH, 'Nadpis může mít maximálně %d znaků', 100)
			->setRequired('Vyplňte nadpis.');

		$form->addTextArea('desc'.$lang, 'Krátký popis:*')
			->addRule(Form::MAX_LENGTH, 'Popis může mít maximálně %d znaků', 1000)
			->setRequired('Vyplňte popis.');

		if ($this->presets->news['richDesc']) {
			$form->addTextarea('text'.$lang, 'Text novinky:')
				->setAttribute('class', 'tinymce');
		}

		$form->addText('video', 'Video:')
			->setAttribute('title', 'Např.: https://www.youtube.com/watch?v=NFZLrfiBTm8')
			->setAttribute('placeholder', 'Např.: https://www.youtube.com/watch?v=NFZLrfiBTm8');

		$form->addMultiUpload('files', 'Soubory')
			->addRule(Form::IMAGE, 'Soubory musí být JPEG, PNG nebo GIF.')
			->addRule(Form::MAX_LENGTH, 'Nahrajte maximálně 3 soubory', 3)
			->setOption('description', Html::el('p')
				->setHtml('<em style="font-style: italic; display: inline-block; padding: 10px 0;line-height: 20px;">Více souborů můžete vybrat při současném držení CTRL. Můžete vybrat maximálně 3 soubory.</em>')
			);

		$form->addCheckbox('top', 'Top - na úvodní stránce? (limit 4)')
			 ->setAttribute('class', 'top-checkbox');

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		$values->date = date("Y-m-d H:i:s",strtotime($values->date));
		

		$id = $this->news->getNextId();

		if (count($values->files) > 0) {
			$files = $values->files;
			foreach ($files as $file) {
				if ($file->isOk()) {
					$path = $this->path . $id . '/' . $file->getSanitizedName();
					$name = $file->getSanitizedName();
					$file->move($path);

					foreach ($this->sizes as $key => $size) {
						$sizes = explode('/', $size);
						$quality = $sizes[2] * 100;
						$image = Image::fromFile($path);
						
						if ($sizes[3] == 'EXACT') {
							$image->resize($sizes[0], $sizes[1], Image::EXACT);	
						} elseif ($sizes[3] == 'FILL') {
							$image->resize($sizes[0], $sizes[1], Image::SHRINK_ONLY | Image::FILL);	
						} elseif ($sizes[3] == 'SHRINK_ONLY') {
							$image->resize($sizes[0], $sizes[1], Image::SHRINK_ONLY);	
						} elseif ($sizes[3] == 'STRETCH') {
							$image->resize($sizes[0], $sizes[1], Image::STRETCH);	
						} else {
							$image->resize($sizes[0], $sizes[1]);	
						} 

						$p = $this->path . $id . '/' . $key . '_' . $name;
						$image->save($p, $quality, Image::JPEG);
					}

					$this->news->addImage($id, $name);
				}
			}
		} 

		unset($values->files);

		if ($values->video) {
			$values->video = self::getVideoId($values->video);
		}

		try {
			$this->news->addNews($values);
		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}


	private function getVideoId($video)
	{
		$video = (string) $video;

		if (strpos($video, 'http://youtu.be/') !== false ) {
			return str_replace('http://youtu.be/', '', $video);
		} 

		if (strpos($video, 'http://www.youtube.com/watch?v=') !== false ) {
			return str_replace('http://www.youtube.com/watch?v=', '', $video);
		}

		if (strpos($video, 'https://www.youtube.com/watch?v=') !== false ) {
			return str_replace('https://www.youtube.com/watch?v=', '', $video);
		} 

		if (strpos($video, 'http://youtube.com/watch?v=') !== false ) {
			return str_replace('http://youtube.com/watch?v=', '', $video);
		} 

		if (strpos($video, 'https://youtube.com/watch?v=') !== false ) {
			return str_replace('https://youtube.com/watch?v=', '', $video);
		} 

		return '';

	}

}
