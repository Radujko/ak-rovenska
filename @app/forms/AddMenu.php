<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tracy\Debugger;
use Nette\Utils\Html;


class AddMenuForm extends Nette\Object
{

	/** @var \App\Model\Menu @inject */
	public $menu;


	public function __construct(\App\Model\Menu $menu)
	{
		$this->menu = $menu;
	}


	/**
	 * @return Form
	 */
	public function create()
	{

		$form = new Form;

		$form->addText('name', 'Název:*')
			->setRequired('Zadejte název menu.');

		$form->addSubmit('save', 'Přidat')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {
			$this->menu->addMenu($values);

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
