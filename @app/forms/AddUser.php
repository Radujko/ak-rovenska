<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tracy\Debugger;
use Nette\Utils\Html;


class AddUserForm extends Nette\Object
{

	/** @var \App\Model\Users @inject */
	public $users;


	public function __construct(\App\Model\Users $users)
	{
		$this->users = $users;
	}


	/**
	 * @return Form
	 */
	public function create()
	{

		$form = new Form;

		$roles = array(
			'admin' => 'admin',
			'editor' => 'editor'
		);

		$form->addText('username', 'Username:*')
			->setRequired('Zadejte uživatelské jméno.');

		$form->addText('name', 'Jméno:');

		$form->addText('email', 'Email:')
			->setType('email');

		$form->addPassword('password', 'Heslo:')
			->setRequired('Zadejte heslo.');

		$form->addPassword('password_check', 'Heslo znovu:')
			->setRequired('Zadejte heslo.');

		$form->addSelect('role', 'Role*:')
			->setItems($roles, FALSE)
			->setPrompt('Vyberte roli')
			->setRequired('Vyberte roli.');

		$form->addCheckbox('status', 'Aktivní:')
			->setValue(1);

		$form->addSubmit('save', 'Přidat')
			->setAttribute('class', 'btn btn-primary');


		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{
		
		if ($values->password == $values->password_check) {
			try {

				$values->created = date("Y-m-d H:i:s",strtotime('now'));

				$this->users->addUser($values);

			} catch (\Exception $e) {
				Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
				$form->addError($e->getMessage());
			}
		} else {
			//throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
			$form->addError('Zadaná hesla se neshodují.');
		}
			
			

		
	}

}
