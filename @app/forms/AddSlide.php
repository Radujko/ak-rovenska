<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\Image;
use Tracy\Debugger;
use Nette\Utils\Html;


class AddSlideForm extends Nette\Object
{

	/** @var \App\Model\Slider @inject */
	public $slider;

	/** @var \App\Model\MenuBuilder @inject */
	public $menuBuilder;

	public $path;

	public $id;


	public function __construct(\App\Model\Slider $slider, \App\Model\MenuBuilder $menuBuilder)
	{
		$this->slider = $slider;
		$this->menuBuilder = $menuBuilder;
	}

	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return Form
	 */
	public function create()
	{

		$form = new Form;

		$form->addHidden('parent_id', $this->id);

		$form->addUpload('image', 'Obrázek')
			->setRequired('Vyberte obrázek.')
			->addRule(Form::IMAGE, 'Soubor musí být JPEG, PNG nebo GIF.');

		$form->addText('url', 'Url:')
			->setAttribute('placeholder', 'http://...');

		$form->addText('p1', 'Text 1:');
		$form->addText('p2', 'Text 2:');
		$form->addText('p3', 'Text 3:');

		$form->addCheckbox('status', 'Aktivní:');

		$form->addSubmit('save', 'Přidat')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		if ($values->image->isOk()) {
			$name = $values->image->getSanitizedName();
			$path = $this->path . 'slides' . '/' . $name;
			$values->image->move($path);

			$image = Image::fromFile($path);

			$image->save($path, 100, Image::JPEG);

			$values->image = $name;
		}

		try {
			$this->slider->addSlider($values);

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
