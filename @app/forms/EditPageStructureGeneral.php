<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tracy\Debugger;
use Nette\Utils\Html;


class EditPageStructureGeneralForm extends Nette\Object
{

	/** @var \App\Model\Structure @inject */
	public $structure;

	public $id;


	public function __construct(\App\Model\Structure $structure)
	{
		$this->structure = $structure;
	}

	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		$page = $this->structure->getPageByKey($this->id);
		$pageData = $this->structure->getPageDataByKey($this->id);

		$robots_select = array(
			'index,follow' => 'index, follow',
			'index,nofollow' => 'index, nofollow',
			'noindex,nofollow' => 'noindex, nofollow',
			'noindex,follow' => 'noindex, follow'
		);


		$form = new Form;


		$form->addHidden('id', $this->id);

		$form->addText('name', 'Název:*')
			->setValue($page->name)
			->setRequired('Zadejte název stránky.');

		$form->addText('url', 'Url adresa:')
			->setValue($pageData->url);

		$form->addSelect('robots', 'Robots:*', $robots_select)
			->setPrompt('-- vyberte meta robots --')
			->setRequired('Vyberte meta robots.')
			->setDefaultValue($pageData->robots);

		$form->addCheckbox('status', 'Aktivní:')
			->setDefaultValue($page->status);

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {
			$key = $values->id;
			unset($values->id);

			$page = [
				'name' => $values->name,
				'status' => $values->status,
			];
			$pageData = [
				'url' => $values->url,
				'robots' => $values->robots
			];

			$this->structure->updatePage($key, $page);
			$this->structure->updatePageData($key, $pageData);

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
