<?php

namespace App\Forms;

use App\Model\Gallery;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\Image;
use Tracy\Debugger;
use Nette\Utils\Html;


class AddGalleryImagesForm extends Nette\Object
{
	
	/** @var Gallery */
	private $gallery;

	/** @var Presets */
	private $presets;

	public $path;
	public $sizes;


	public function __construct(\App\Model\Gallery $gallery, \App\Model\Presets $presets)
	{
		$this->presets = $presets->getPresets();
		$this->gallery = $gallery;
	}


	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}

	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		
		$form = new Form;

		$max = 999 - $this->gallery->getImages($this->id)->count();

		$form->addMultiUpload('files', 'Soubory')
			->setAttribute('class', 'image-input')
			->addRule(Form::IMAGE, 'Soubory musí být JPEG, PNG nebo GIF.')
			->addRule(Form::MAX_LENGTH, 'Nahrajte maximálně %d soubory', $max)
			->setOption('description', Html::el('p')
				->setHtml('<em style="font-style: italic; display: inline-block; padding: 10px 0;line-height: 20px;">Více souborů můžete vybrat při současném držení CTRL. Nyní můžete vybrat maximálně '.$max.' soubor/y.</em>')
			);
			

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		$id = $this->id;

		if (count($values->files) > 0) {
			$files = $values->files;
			foreach ($files as $file) {
				if ($file->isOk()) {
					$path = $this->path . 'g' . $id . '/' . $file->getSanitizedName();
					$name = $file->getSanitizedName();
					$ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));
					$file->move($path);

					foreach ($this->sizes as $key => $size) {
						$sizes = explode('/', $size);
						$quality = $sizes[2] * 100;
						$image = Image::fromFile($path);
						
						if ($sizes[3] == 'EXACT') {
							$image->resize($sizes[0], $sizes[1], Image::EXACT);	
						} elseif ($sizes[3] == 'FILL') {
							$image->resize($sizes[0], $sizes[1], Image::SHRINK_ONLY | Image::FILL);	
						} elseif ($sizes[3] == 'SHRINK_ONLY') {
							$image->resize($sizes[0], $sizes[1], Image::SHRINK_ONLY);	
						} elseif ($sizes[3] == 'STRETCH') {
							$image->resize($sizes[0], $sizes[1], Image::STRETCH);	
						} else {
							$image->resize($sizes[0], $sizes[1]);	
						} 

						$p = $this->path . 'g' . $id . '/' . $key . '_' . $name;
						$image->save($p, $quality, (($ext == 'png')?Image::PNG:Image::JPEG));
					}

					$this->gallery->addImage($id, $name);
				}
			}
		} 

	}




}
