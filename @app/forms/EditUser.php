<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tracy\Debugger;
use Nette\Utils\Html;


class EditUserForm extends Nette\Object
{

	/** @var \App\Model\Users @inject */
	public $users;


	public function __construct(\App\Model\Users $users)
	{
		$this->users = $users;
	}


	/**
	 * @return Form
	 */
	public function create()
	{

		$form = new Form;

		$roles = array(
			'admin' => 'admin',
			'test' => 'test'
		);

		$form->addHidden('id','0');

		$form->addText('username', 'Username:*')
			->setRequired('Zadejte uživatelské jméno.');

		$form->addText('name', 'Jméno:');

		$form->addText('email', 'Email:')
			->setType('email');

//		$form->addPassword('password', 'Heslo:')
//			->setRequired('Zadejte uživatelské jméno.');

		$form->addSelect('role', 'Role*:')
			->setItems($roles, FALSE)
			->setPrompt('Vyberte roly')
			->setRequired('Vyberte roly.');

		$form->addCheckbox('status', 'Aktivní:');

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');


		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {
			$key = $values->id;
			unset($values->id);

			$this->users->updateUser($key, $values);

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
