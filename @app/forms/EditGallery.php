<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\Strings;
use Nette\Utils\Image;
use Tracy\Debugger;


class EditGalleryForm extends Nette\Object
{

	/** @var Gallery */
	private $gallery;

	/** @var Presets */
	private $presets;

	/** @var Sizes */
	private $sizes;

	public $path;
	public $id;
	public $lang;



	public function __construct(\App\Model\Gallery $gallery, \App\Model\Presets $presets)
	{
		$this->presets = $presets->getPresets();
		$this->gallery = $gallery;
	}



	public function setId($id)
	{
		$this->id = $id;
	}

	public function setPath($path)
	{
		$this->path = $path;
	}

	public function setSizes($sizes)
	{
		$this->sizes = $sizes;
	}


	/**
	 * @return Form
	 */
	public function create($lang)
	{
		
		$gallery = $this->gallery->getPage($this->id);
		$lang = '_' . Strings::truncate($lang,2,'');
		$this->lang = $lang;

		$form = new Form;

		$form->addHidden('id', $this->id);



		$form->addText('date', 'Datum:*')
			->setAttribute('class', 'date')
			->setRequired('Vyplňte datum.')
			->setAttribute('id','dateNew');

		$form->addCheckbox('category_foto', 'Foto')
			->setAttribute('class', 'checkbox');
		$form->addCheckbox('category_video', 'Video')
			->setAttribute('class', 'checkbox');
		$form->addCheckbox('category_kultura', 'Kultura')
			->setAttribute('class', 'checkbox');
		$form->addCheckbox('category_sport', 'Sport')
			->setAttribute('class', 'checkbox');

		$form->addText('headline'.$lang, 'Nadpis:*')
			->addRule(Form::MAX_LENGTH, 'Nadpis může mít maximálně %d znaků', 100)
			->setRequired('Vyplňte nadpis.');

		if ($this->presets->news['richDesc']) {
			$form->addTextarea('text'.$lang, 'Text galerie:')
				->setAttribute('class', 'tinymce');
		}

		$form->addText('video', 'Video:')
			->setAttribute('title', 'Např.: https://www.youtube.com/watch?v=NFZLrfiBTm8')
			->setAttribute('placeholder', 'Např.: https://www.youtube.com/watch?v=NFZLrfiBTm8');

//		$form->addCheckbox('top', 'Top')
//			 ->setAttribute('class', 'top-checkbox');

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->setDefaults($gallery);

		$form['date']->setDefaultValue(date("j.n.Y",strtotime($gallery->date)));

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		$values->date = date("Y-m-d",strtotime($values->date));


		if ($values->video) {
			$values->video = self::getVideoId($values->video);
		}


		try {
			$this->gallery->editPage($values);
		} catch (\Exception $e) {
			Debugger::log('Problem with edit new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}


	private function getVideoId($video)
	{
		$video = (string) $video;

		if (strpos($video, 'http://youtu.be/') !== false ) {
			return str_replace('http://youtu.be/', '', $video);
		} 

		if (strpos($video, 'http://www.youtube.com/watch?v=') !== false ) {
			return str_replace('http://www.youtube.com/watch?v=', '', $video);
		}

		if (strpos($video, 'https://www.youtube.com/watch?v=') !== false ) {
			return str_replace('https://www.youtube.com/watch?v=', '', $video);
		} 

		if (strpos($video, 'http://youtube.com/watch?v=') !== false ) {
			return str_replace('http://youtube.com/watch?v=', '', $video);
		} 

		if (strpos($video, 'https://youtube.com/watch?v=') !== false ) {
			return str_replace('https://youtube.com/watch?v=', '', $video);
		} 

		return '';

	}

}
