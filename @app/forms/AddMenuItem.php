<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tracy\Debugger;
use Nette\Utils\Html;


class AddMenuItemForm extends Nette\Object
{

	/** @var \App\Model\Menu @inject */
	public $menu;

	/** @var \App\Model\MenuBuilder @inject */
	public $menuBuilder;

	public $structureItems = array();

	public $structureItemsLvl = 0;

	public $id;


	public function __construct(\App\Model\Menu $menu, \App\Model\MenuBuilder $menuBuilder)
	{
		$this->menu = $menu;
		$this->menuBuilder = $menuBuilder;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	private function howToReadSubs()
	{
		$str = '';
		for ($i = 1; $i <= $this->structureItemsLvl; $i++) {
			$str .= '- ';
		}

		return $str;
	}

	private function getStructureOptionsSubs($items)
	{
		if (empty($items) === false) {
			foreach ($items as $i) {
				$this->structureItemsLvl++;
				$this->structureItems[$i->id] = $this->howToReadSubs().$i->name;
				if (empty($i->subs) === false) {
					$this->getStructureOptionsSubs($i->subs);
				}
				$this->structureItemsLvl--;
			}
		}
	}

	private function getStructureOptions($items)
	{
		$this->structureItemsLvl = 1;

		foreach ($items as $i) {
			$this->structureItems[$i->id] = $this->howToReadSubs().$i->name;
			if (empty($i->subs) === false) {
				$this->getStructureOptionsSubs($i->subs);
			}
		}
	}

	/**
	 * @return Form
	 */
	public function create()
	{

		$form = new Form;


		$this->menuBuilder->table_name = 'structure';
		$structureItems = $this->menuBuilder->getArray(1);

		$this->getStructureOptions($structureItems);

//		print_r($this->structureItems);exit();


		$form->addMultiSelect('pages', 'Položky:*', $this->structureItems)
			->setAttribute('data-placeholder', '...')
			->setAttribute('multiple', 'multiple')
			->setAttribute('class', 'chosen-select-page')
			->setRequired('Vyberte položku.');

		$form->addHidden('itemId',$this->id);

		$form->addSubmit('save', 'Přidat')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {
			$this->menu->addMenuItem($values);

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
