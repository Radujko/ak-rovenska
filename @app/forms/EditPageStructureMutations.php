<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;
use Nette\Security\User;
use Tracy\Debugger;
use Nette\Utils\Html;


class EditPageStructureMutationsForm extends Nette\Object
{

	/** @var \App\Model\Structure @inject */
	public $structure;

	public $id;

	public $lang;


	public function __construct(\App\Model\Structure $structure)
	{
		$this->structure = $structure;
	}

	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return Form
	 */
	public function create($lang)
	{
		$lang = '_' . Strings::truncate($lang,2,'');
		$pageData = $this->structure->getPageDataByKey($this->id);
		$this->lang = $lang;


		$form = new Form;


		$form->addHidden('id', $this->id);

		$form->addText('menuText'.$lang, 'Název v menu:')
			->setValue($pageData['menuText'.$lang]);

		$form->addText('title'.$lang, 'Titulek:')
			->setValue($pageData['title'.$lang]);

		$form->addText('h1'.$lang, 'H1:')
			->setValue($pageData['h1'.$lang]);

		$form->addText('h2'.$lang, 'H2:')
			->setValue($pageData['h2'.$lang]);

		$form->addTextArea('description'.$lang, 'META description:')
			->setValue($pageData['description'.$lang]);

		$form->addTextArea('keywords'.$lang, 'META keywords:')
			->setValue($pageData['keywords'.$lang]);

		$form->addTextArea('anotace'.$lang, 'Anotace:')
			->setValue($pageData['anotace'.$lang]);

		$form->addTextArea('text1'.$lang, 'Text1:')
			->setValue($pageData['text1'.$lang])
			->setAttribute('class', 'tinymce');

		$form->addTextArea('text2'.$lang, 'Text2:')
			->setValue($pageData['text2'.$lang])
			->setAttribute('class', 'tinymce');

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {
			$key = $values->id;
			unset($values->id);

			$this->structure->updatePageData($key, $values);

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
