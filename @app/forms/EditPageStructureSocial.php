<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tracy\Debugger;
use Nette\Utils\Html;


class EditPageStructureSocialForm extends Nette\Object
{

	/** @var \App\Model\Structure @inject */
	public $structure;

	public $id;


	public function __construct(\App\Model\Structure $structure)
	{
		$this->structure = $structure;
	}

	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		$pageData = $this->structure->getPageDataByKey($this->id);


		$form = new Form;


		$form->addHidden('id', $this->id);

		$form->addText('ogTitle', 'Titulek:')
			->setValue($pageData->ogTitle)
			->setAttribute('placeholder', 'og:title');

		$form->addText('ogType', 'Typ:')
			->setValue($pageData->ogType)
			->setAttribute('placeholder', 'og:type');

		$form->addTextArea('ogDescription', 'Popis:')
			->setValue($pageData->ogDescription)
			->setAttribute('placeholder', 'og:description')
			->addRule(Form::MAX_LENGTH, 'Poznámka je příliš dlouhá', 2047);

		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = array($this, 'formSucceeded');

		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{

		try {
			$key = $values->id;
			unset($values->id);

			$this->structure->updatePageData($key, $values);

		} catch (\Exception $e) {
			Debugger::log('Problem with add new: ' . $e->getMessage(), Debugger::ERROR);
			$form->addError($e->getMessage());
		}
	}

}
