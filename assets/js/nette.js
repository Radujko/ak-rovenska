$(function() {
	///// NETTE AJAX /////

	$('form.ajaxForce').on('submit', function (e) {
		$(this).netteAjax(e, {
			validate: {
				url: false
			}
		});
	});

	$(document).on("click", 'a.ajaxForce', function (e) {
		$(this).netteAjax(e, {
			validate: {
				url: false
			}
		});
	});

	$.nette.ext('binder', {
		success: function () {
			initSelects();
			if (typeof initPriceRangeSlider === "function") {
				initPriceRangeSlider($);
			}
			if (typeof initDynamicSliders === "function") {
				initDynamicSliders($);
			}
			if (typeof initFilterCheckboxes === "function") {
				initFilterCheckboxes($);
			}
		}
	});

	$.nette.ext('spinner', {
		start: function () {
			$('html').addClass('wait');
			new ajaxLoader('.ajaxloader'); //destroyed after payload redraw
		},
		complete: function () {
			$('html').removeClass('wait');
		}
	});

	$.nette.ext('forceRedirect', {
		success: function (payload) {
			if (payload.forceRedirect) {
				window.location.href = payload.forceRedirect;
				return false;
			}
		}
	});

	$.nette.ext('history').cache = false;
	$.nette.init();

})