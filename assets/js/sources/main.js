function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length,c.length);
		}
	}
	return "";
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

$(function(){
	$(document).ready(function(){
  	handleNavBurger();
    closeModal();
    closeFlashMessage()
  });

	baguetteBox.run('.baguetteBoxOne');


	$('.arrowToScroll').click(function(e){
		e.preventDefault();

		$('html, body').animate({
			scrollTop: $(this).offset().top
		}, 500);
	});

	// active menu
	var activePageId = $('body').attr('data-id');
	$('ul li[data-id="'+activePageId+'"]').addClass('active').parents('li').addClass('active');

	// burger menu
	$('.header .second-row .head-nav nav').clone().appendTo('.header .wrapper.navigate').addClass('mobile').fadeOut(0);

	$('.head-nav .burgetmenu').click(function(){
		$(this).find('a').toggleClass('active');
		$('.header nav.mobile').slideToggle(300);
	});

	// burger menu toggle show subs
	$('.header nav.mobile li').each(function(){
		if ($(this).children('ul').length > 0) {
			$('<span class="showSubs"></span>').appendTo(this);
		}
	});

	$('.header nav.mobile span.showSubs').click(function(){
		$(this).toggleClass('active');
		$(this).siblings('ul').slideToggle(300);
	});

	$('.header nav.mobile li.active').each(function(){
		if ($(this).children('ul').find('li.active').length > 0) {
			$(this).children('span.showSubs').addClass('active');
			$(this).children('ul').fadeIn(0);
		}
	});


	// onlick scroll
	$('[data-scroll]').click(function(e){
		e.preventDefault();

		var target = $(this).attr('data-scroll');
		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 500);
	});

	// vypsání sideNav
	var $targetEle = $('.sideNav li[data-id="'+activePageId+'"]'),
		depthEle = parseInt($targetEle.parents('ul').length);

	if ($targetEle.length > 0) {
		if (depthEle == 1 && $targetEle.children('ul').length > 0) {
			var getUl = $targetEle.children('ul').get(0).outerHTML;
			$('.sideNav').html(getUl).fadeIn(0);
		}
		else if (depthEle == 2) {
			var getUl = $targetEle.closest('ul').get(0).outerHTML;
			$('.sideNav').html(getUl).fadeIn(0);
		}
		else {
			if ($targetEle.closest('ul').closest('li').closest('ul').length > 0) {
				var getUl = $targetEle.closest('ul').closest('li').closest('ul').get(0).outerHTML;
				$('.sideNav').html(getUl).fadeIn(0);
			}
		}
	}

	// vysouvací fixní menu
	$('.header .topstrip .second-row').clone().appendTo('.header .topstrip .wrapper').addClass('appended fx').css('display','none');

	var  MAXt = 250;
	var setPosition = function () {
		var scrollTop = $(window).scrollTop();
		if (scrollTop > MAXt) {
			$('.header .topstrip .second-row.appended').slideDown(300);
		} else {
			$('.header .topstrip .second-row.appended').slideUp(100);
		}
	};

	$(window).bind('scroll', setPosition);
	setPosition();

	// Aktuální počet návštěvníků bazénového centra
	if ($(".bvaktnum").length > 0) {
		$.ajax({
			type: "GET",
			url: "/bv.php",
			data: "bvakt=1",
			cache: false,
			success: function(data){
				$(".bvaktnum").text(data);
			}
		})
	}

	// setnutí news filtrů podle cookie
	$('input[type="checkbox"].news-filter').each(function(){
		var name = $(this).attr('name'),
			state = pomocnik = getCookie(name);

		if (!state || state == 'true') {
			$(this).prop('checked', true);
		}//console.log(name+': '+state);
	});
	// event na change
	$('input[type="checkbox"].news-filter').bootstrapSwitch();
	$('input[type="checkbox"].news-filter').on('switchChange.bootstrapSwitch', function(event, state) {
		var target = $(this).attr('name');

		setCookie(target,state,20);
		location.reload();
	});
	// toggle animate change
	$("[data-switch-toggle]").on("click", function() {
		var target = $(this).data("switch-toggle");
		return $("#switch-" + target).bootstrapSwitch("toggleState");
	});
});

function closeModal() {
  var close = $('.close'),
    overlay = $('#overlay'),
    modal = $('.modal');
  close.click(function() {
    modal.hide();
    overlay.hide();
  });

  overlay.click(function() {
    modal.hide();
    overlay.hide();
  });
}

function closeFlashMessage() {
  $('.flash .close').click(function(){
    $('.flash').slideUp();
  });
}

function handleNavBurger() {
  var $burger = $('.nav-burger'),
      $nav = $('.nav-ul');
    $burger.click(function(event) {
      $burger.toggleClass('active');
      if ($burger.hasClass('active')) {
        $nav.slideDown(200);
      } else {
        $nav.slideUp(50);
      }
    });
}
