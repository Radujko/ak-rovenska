$(document).ready(function(){
	$('.flash .close').click(function(){
		$('.flash').slideUp();
	});

	$('.open-modal').click(function(){
		var target = $(this).attr('data-target');
		$('.modal-overlay').show();
		$('.'+target).fadeIn();
	});

	$(".hasDatepicker").datepicker();

	$('.modal-close').click(function(){
		$('.modal').fadeOut();
		$('.modal-overlay').fadeOut();
	});

	$('.nav-tabs a').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
	});

	$('.datatable.desc').DataTable({
		"order": [[ 0, "desc" ]],
		"lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "Vše"]],
		"language": {
			"lengthMenu": "Zobraz _MENU_ záznamů na stránce",
			"zeroRecords": "Tabulka neobsahuje žádné záznamy",
			"info": "Zobrazuji stránku _PAGE_ z _PAGES_",
			"infoEmpty": "Žádné záznamy",
			"infoFiltered": "(fitrováno z celkového počtu _MAX_ záznamů)",
			"sLoadingRecords": "Načítám...",
			"sProcessing":     "Provádím...",
			"sSearch":         "Hledat:",
			"oPaginate": {
				"sFirst":    "První",
				"sLast":     "Poslední",
				"sNext":     "Další",
				"sPrevious": "Předchozí"
			}
		}
	});

	$('.datatable.asc').DataTable({
		"order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "Vše"]],
		"language": {
			"lengthMenu": "Zobraz _MENU_ záznamů na stránce",
			"zeroRecords": "Tabulka neobsahuje žádné záznamy",
			"info": "Zobrazuji stránku _PAGE_ z _PAGES_",
			"infoEmpty": "Žádné záznamy",
			"infoFiltered": "(fitrováno z celkového počtu _MAX_ záznamů)",
			"sLoadingRecords": "Načítám...",
			"sProcessing":     "Provádím...",
			"sSearch":         "Hledat:",
			"oPaginate": {
				"sFirst":    "První",
				"sLast":     "Poslední",
				"sNext":     "Další",
				"sPrevious": "Předchozí"
			}
		}
	});


	// auto otevírání tabů přes get (po kliku ze změní url)
	var $tabToOpen = $('.nav-tabs a[href="#' + getUrlParameter('tab') + '"]');
	if ( $tabToOpen ) {
		$tabToOpen.tab('show');
	}
	$('.nav-tabs a[href^="#"]').click(function(e){
		var target = $(this).attr('href').substring(1);
		setGetParameter('tab',target);
	});


	// otevření modalu pokud je v něm chybná hláška
	if ($('ul.error').length > 0) {

		var id = $('ul.error').closest('.modal').attr('id');
		console.log(id);
		$('#'+id).modal('show');
	}
});

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
		{
			return sParameterName[1];
		}
	}
}

function setGetParameter(paramName, paramValue) {
	var url = window.location.href.replace(window.location.hash, '');
	if (url.indexOf(paramName + "=") >= 0) {
		var prefix = url.substring(0, url.indexOf(paramName));
		var suffix = url.substring(url.indexOf(paramName));
		suffix = suffix.substring(suffix.indexOf("=") + 1);
		suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
		url = prefix + paramName + "=" + paramValue + suffix;
	}else {
		if (url.indexOf("?") < 0)
			url += "?" + paramName + "=" + paramValue;
		else
			url += "&" + paramName + "=" + paramValue;
	}
	url += window.location.hash;
	history.pushState({}, null, url);
}

/* Czech initialisation for the jQuery UI date picker plugin. */
/* Written by Tomas Muller (tomas@tomas-muller.net). */
( function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define( [ "../widgets/datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}( function( datepicker ) {

datepicker.regional.cs = {
	closeText: "Zavřít",
	prevText: "&#x3C;Dříve",
	nextText: "Později&#x3E;",
	currentText: "Nyní",
	monthNames: [ "leden","únor","březen","duben","květen","červen",
	"červenec","srpen","září","říjen","listopad","prosinec" ],
	monthNamesShort: [ "led","úno","bře","dub","kvě","čer",
	"čvc","srp","zář","říj","lis","pro" ],
	dayNames: [ "neděle", "pondělí", "úterý", "středa", "čtvrtek", "pátek", "sobota" ],
	dayNamesShort: [ "ne", "po", "út", "st", "čt", "pá", "so" ],
	dayNamesMin: [ "ne","po","út","st","čt","pá","so" ],
	weekHeader: "Týd",
	dateFormat: "dd.mm.yy",
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: "" };
datepicker.setDefaults( datepicker.regional.cs );

return datepicker.regional.cs;

} ) );

$(function(){
	  
	
});