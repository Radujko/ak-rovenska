<script type="text/javascript">

var allowClass = "p,span,div,a,b,strong,i,u,li,ol,ul,label,img,h2,h3,h4,h5,h6";

tinymce.init({
    selector: "textarea.tinymce",
    browser_spellcheck : true,
    theme: "modern",
    language : "cs",
    width: 670,
    height: 450,
    plugins: [
	" advlist nonbreaking autolink link image lists charmap print preview hr anchor pagebreak",
	"searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
	"table contextmenu directionality emoticons paste textcolor responsivefilemanager code codemirror colorpicker"
],
    resize: "both",
    autoresize_min_height: 500,
    autoresize_max_height: 10000,

    content_css: "<?php echo PATH_WWW ?>/core/js/tinymce/css/clanek.css,<?php echo PATH_WWW ?>/core/js/tinymce/css/styles.css",
    relative_urls: false,
    convert_urls: false,
    nonbreaking_force_tab : true,
    remove_script_host : false,
    entity_encoding : "raw",
    filemanager_title:"Responsive Filemanager",
    external_filemanager_path:"<?php echo PATH_WWW ?>/core/js/tinymce/filemanager/",
    link_class_list: [
        {title: "Žádné", value: ""},
        {title: "Galerie", value: "fancybox"}
    ],
    external_plugins: { "filemanager" : "<?php echo PATH_WWW ?>/core/js/tinymce/filemanager/plugin.min.js"},
    codemirror: {
	indentOnInit: true, // Whether or not to indent code on init.
    path: "<?php echo PATH_WWW ?>/core/js/tinymce/CodeMirror",
  },
  
    style_formats : [
        {title : "nadp1", classes : "nadp1", selector: allowClass},
        {title : "text_bigger", classes : "text_bigger", selector: allowClass}

    ],


   image_advtab: true,
   toolbar1: "undo redo | pastetext pasteword | bold italic underline strikethrough | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent  ",
   toolbar2: "styleselect | formatselect | responsivefilemanager | image | media | link unlink | removeformat | attribs | print preview code "
 });
</script>