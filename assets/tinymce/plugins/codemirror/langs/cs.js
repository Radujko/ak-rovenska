tinymce.addI18n('cs',{
	'HTML source code': 'HTML zdrojový kod',
	'Start search': 'Najít',
	'Find next': 'Najít další',
	'Find previous': 'Najít předchozí',
	'Replace': 'Nahradit',
	'Replace all': 'Nahradit vše'
});
