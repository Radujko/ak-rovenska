/*
 * grunt-cli
 * http://gruntjs.com/
 *
 * Copyright (c) 2012 Tyler Kellen, contributors
 * Licensed under the MIT license.
 * https://github.com/gruntjs/grunt-init/blob/master/LICENSE-MIT
 */

'use strict';

module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
   
  grunt.initConfig({
    watch: {
      scripts: {
        files: ['assets/less/*.less', 'assets/less/**/*.less', 'assets/js/sources/*.js'],
        tasks: ['less', 'cssmin'],
        options: {
          spawn: true
        },
      }
    },

    less: {
      development: {
        options: {
          paths: ["assets/css", "assets/less"],
          compress: true
        },
        files: {
          "assets/css/sandbox.css": "assets/less/index.less",
          "assets/css/admin.css": "assets/less/admin.less"
        }
      } 
    },

    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          "assets/css/sandbox.min.css": [ "assets/css/sandbox.css" ],
          "assets/css/admin.min.css": [ "assets/css/admin.css" ]
        }
      }
    },
     
    uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: {
          'assets/js/sandbox.min.js': [
                'assets/js/sources/jquery-1.11.3.min.js',
                'assets/js/sources/moment.js',
                'assets/js/sources/jquery.eventCalendar.min.js',
                'assets/js/sources/bootstrap-switch.min.js',
                'assets/js/sources/baguetteBox.min.js',
                'assets/js/sources/netteForms.js',
                'assets/js/sources/main.js'
            ],
          'assets/js/admin.min.js': [
                'assets/js/sources/jquery-1.11.3.min.js',
                'assets/js/sources/bootstrap.min.js',
                'assets/js/sources/jquery-ui.min.js',
                'assets/js/sources/moment.js',
                'assets/js/sources/bootstrap-datetimepicker.js',
                'assets/js/sources/jquery-sortable-min.js',
                'assets/js/sources/chosen.jquery.min.js',
                'assets/js/sources/datatable.js',
                'assets/js/sources/slick.js',
                'assets/js/sources/netteForms.js',
                'assets/js/nette.ajax.js',
                'assets/js/sources/admin.js'
            ]
        }
      }
    }
  });

  grunt.registerTask('default', ['less']);
};
